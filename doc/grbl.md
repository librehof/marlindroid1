# GRBL g-code notes


## Status

- **Idle**
- **Run**
- **Jog**
- **Hold** (pause)
- **error:**\<number\>


## Special commands

### $# - View gcode parameters

G-code parameters store the coordinate offset values for G54-G59 work coordinates

\[G54:4.000,0.000,0.000\]  
\[G55:4.000,6.000,7.000\]  
\[G56:0.000,0.000,0.000\]  
\[G57:0.000,0.000,0.000\]  
\[G58:0.000,0.000,0.000\]  
\[G59:0.000,0.000,0.000\]  
\[G28:1.000,2.000,0.000\]  
\[G30:4.000,6.000,0.000\]  
\[G92:0.000,0.000,0.000\]  
\[TLO:0.000\]  
\[PRB:0.000,0.000,0.000:0\]  


### $G - View gcode parser state

This command prints all of the active gcode modes in Grbl's G-code parser. When sending this command to Grbl, it will reply with a message starting with [GC: ... ]

[GC:G0 G54 G17 G21 G90 G94 M0 M5 M9 T0 S0.0 F500.0]

These **active modes** determine how the next G-code block or command will be interpreted by Grbl's G-code parser

| Modal Group 	              | Member Words                                          
|:----------------------------|:------------------------------------------------------
| Motion Mode                 | G0, G1, G2, G3, G38.2, G38.3, G38.4, G38.5, G80
| Coordinate System Select    | G54, G55, G56, G57, G58, G59
| Plane Select                | G17, G18, G19
| Distance Mode               | G90, G91
| Arc IJK Distance Mode       | G91.1
| Feed Rate Mode              | G93, G94
| Units Mode                  | G20, G21
| Cutter Radius Compensation  | G40
| Tool Length Offset          | G43.1, G49
| Program Mode                | M0, M1, M2, M30
| Spindle State               | M3, M4, M5
| Coolant State               | M7, M8, M9

In addition to the G-code parser modes, Grbl will report the active **T** tool number, **S** spindle speed, and **F** feed rate, which all default to 0 upon a reset.


### $X - Kill alarm lock

Grbl's alarm mode is a state when something has gone critically wrong, such as a hard limit or an abort during a cycle, or if Grbl doesn't know its position. By default, if you have homing enabled and power-up the Arduino, Grbl enters the alarm state, because it does not know its position. The alarm mode will lock all G-code commands until the '$H' homing cycle has been performed. Or if a user needs to override the alarm lock to move their axes off their limit switches, for example, '$X' kill alarm lock will override the locks and allow G-code functions to work again.


### $H - Run homing cycle

This command is the only way to perform the homing cycle in Grbl. Some other motion controllers designate a special G-code command to run a homing cycle, but this is incorrect according to the G-code standards. Homing is a completely separate command handled by the controller.


### $J=line - Run jogging motion

New to Grbl v1.1, this command will execute a special jogging motion. There are three main differences between a jogging motion and a motion commanded by a g-code line.


## Realtime commands

Realtime commands are single control characters that may be sent to Grbl to command and perform an action in real-time.

- **0x18** (ctrl-x) : Soft-Reset
  - Immediately halts and safely resets Grbl without a power-cycle
- **?** : Status Report Query
  - Immediately generates and sends back runtime data with a status report
- **~** : Cycle Start / Resume
  - Resumes a feed hold
- **!** : Feed Hold
  - Places Grbl into a suspend or HOLD state. 
  - If in motion, the machine will decelerate to a stop and then be suspended
  - Command executes when Grbl is in an IDLE, RUN, or JOG state. It is otherwise ignored
