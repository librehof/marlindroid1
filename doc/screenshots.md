### MarlinDroid 1 | screenshots

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)


## DRO view
![Status](status.jpg)


## Table view

![Table](table.jpg)
