@echo off
set METHOD=%1
set MODE=%2

cd /D "%~dp0"

if not exist "log" mkdir log
if not exist "local" mkdir local
echo "WIN" > local/.bound

python source\marlindroid.py %METHOD% %MODE%
