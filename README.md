# MarlinDroid 1

GPLv3 (C) 2021 [librehof.com](http://librehof.com)


## Summary

- Use a control board with **[Marlin firmware](https://marlinfw.org)** (3D-printer focused) to control your CNC
- MarlinDroid sits **between** your g-code client and your machine
  - Use a **GRBL** g-code client (CNC focused) like **[bCNC](https://github.com/vlachoudis/bCNC)**, and have MarlinDroid translate from GRBL to Marlin
- MarlinDroid also provides a simple web interface with a **DRO** and a **Table** view
- With Marlin firmware you can:  
  - use a **powerfull, affordable and compact** control board for your CNC
  - use pluggable **Trinamic** drivers like the powerfull TMC5160
  - see **[marlincncifier](http://librehof.com)** to prepare custom Marlin firmware for your CNC
- Uses the [millipy](http://librehof.com) framework
- **[Screenshots](doc/screenshots.md)**
- [Changelog](doc/changelog.md)
- [Legal notice](NOTICE)


## Workspaces (grids)

- G54 serves as the **machine grid** once homed
- G55-G59 are **local grids** with programmable offsets to G54
- offsets are saved and remembered in `data/workspace.ini`  
- the workspace logic is handled by MarlinDroid
- do not enable workspace support in Marlin


## Requirements

- install git
  - on linux use your package manager, see [command-not-found.com](https://command-not-found.com/git)
  - on windows goto [gitforwindows.org](https://gitforwindows.org)
- install python 3.6+
  - on linux use your package manager, see [command-not-found.com](https://command-not-found.com/python3)
  - on windows download installer from [www.python.org](https://www.python.org/downloads/windows/)
  - when installing on windows, make sure the **python** command is available from the cmd prompt
- install pyserial
  - on linux: 
    - make sure you have pip3: see [command-not-found.com](https://command-not-found.com/python3-pip)
    - pip3 install pyserial
  - on windows:
    - pip install pyserial
- on windows: use/install an editor like notepad++ for editing config files


## Download and try

```
# first time download
git clone --depth 10 https://gitlab.com/librehof/marlindroid1.git

# try in shell
cd marlindroid1
./marlindroid1

# on window you can click on "marlindroid1.bat" to start

# open user interface in your browser: 
# http://localhost:8000
# or http://127.0.0.1:8000 

# ctrl-c to abort in shell
```


## Use
```
# run once in shell, ctrl-c, then...

# review config in the config directory
# like FIRMWARETTY in app.ini

# see setup help page for options
./setup -h

# register as a service (linux):
./setup service

# setup apache or nginx proxy (linux):
./setup proxy

# run from menu or desktop (linux)
./setup menuicon
./setup deskicon

# open user interface in your browser: http://localhost:8000 
# connect your g-code client (raw socket): localhost:2000

# update
cd marlindroid1
./unit update

# remove
cd marlindroid1
./teardown
# perhaps backup your config then...
cd ..
rm -rf marlindroid1
```


## Status

- Idle
- Run (G1-Gn)
- Jog (G0)
- Hold (pause)
- Offline
- Check (End-stops disabled)
- POS (pos changed without G0/G1/Gn, usually when changing grid)  
- LIMITSTOP (endstop signal, requires reset or re-connect)


## Machine notes

- Tested with Marlin 2.0.6.1 using a LPC1768/9 board with TMC5160 drivers
  - End-stops, StallGuard signal, and Emergency stop button wired together
  - Using bCNC g-code client (GRBL focused)  
- **Homing**
  - When combining StallGuard and physical End-stops, Marlin's backoff feature will not work
  - Try a sequence like this (example with negative X and negative Y End-stops):
    ```
    G54        ; machine grid
    M121       ; use end-stops only when homing, danger! 
    G28 X Y    ; home XY
    G92 X0 Y0  ; zero
    G0 X3 Y3   ; backoff from negative end-stops
    G4 P1000   ; delay (give time for backoff)
    M120       ; enable end-stops again
    ```
- **End-stops**
  - MarlinDroid will enable End-stops when connecting to Marlin
  - When you get an **unexpected stop** (triggered by an End-stop signal when not homing):
    - **REBOOT Marlin board and redo HOMING**
- **Stop** via g-gode client:
  - MarlinDroid will make a "Marlin" emergency stop
  - after **stop**
    - **REBOOT Marlin board and redo HOMING**
- **Pause**
  - MarlinDroid will pause sending of g-code, however, g-code already sent to Marlin's g-code buffer will be executed before pause
- When disabling (M18), then enabling (M17) stepper motors
  - Be aware that precision can be lost
- Spindle speed is not reflected back to the GRBL client (TODO)
- Same goes for laser (TODO)


## bCNC

Preferably install [bCNC](https://github.com/vlachoudis/bCNC) together with MarlinDroid

Install dependencies:
```
- tkinter the graphical toolkit for python

depending your python/OS it can either be already installed, 
or under the names tkinter, python3-tkinter, python-tk

- pyserial or under the name python-serial, python-pyserial

- numpy

Optionally:
- python-imaging-tk: the PIL libraries for autolevel height map
- python-opencv: for webcam streaming on web pendant
- scipy: for 100 times faster 3D mesh slicing
```

You can run directly from source:
```
git clone --depth 10 https://github.com/vlachoudis/bCNC.git
cd bCNC
python -m bCNC
```

Enable 6 Axis under CAM/Config to access the A-axis (E-axis on a 3D printer)  
Use connection string: `socket://localhost:2000`
