#
# Pass-through without workspace logic (wire tapping)
#
# GPLv3 (C) 2021 librehof.com
#

from app import *

#==========================================================

class Simple:

  def __init__(self, clientio, firmwareio, innerlog):

    self.clientio = clientio
    self.firmwareio = firmwareio
    self.innerlog = innerlog

  #--------------------------------------------------------

  def run(self):

    while True:
      milliping(1)

      # client receive, firmware send

      self.clientio.receive()

      # special grbl status handling
      gstatus = False
      while self.clientio.getmark("?"):
        gstatus = True # group status requests together
      if gstatus:
        self.firmwareio += "?"

      while True:
        req = self.clientio.getbymark("\n")
        if not req:
          break
        req = gcode_normalizereq(req)
        self.innerlog.addline(req)
        self.firmwareio += f"{req}\n"

      # firmware receive, client send

      self.firmwareio.receive()

      while True:
        resp = self.firmwareio.getbymark("\n")
        if not resp:
          break
        if not resp.startswith("<"):
          self.innerlog.addline(resp)
        self.clientio += f"{resp}\n" # will be translated to \r\n

#==========================================================
