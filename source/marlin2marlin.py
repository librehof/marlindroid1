#
# Marlin client <=> Marlin server (pass-through)
#
# GPLv3 (C) 2021 librehof.com
#

from app import *
from marlincaller import *

#==========================================================

class Marlin2Marlin:

  def __init__(self, clientio, marlincaller, innerlog):

    self.clientio = clientio
    self.marlincaller = marlincaller
    self.innerlog = innerlog

  #--------------------------------------------------------

  def reply(self, mresplines):

    mrespblock = ""
    for mrespline in mresplines:
      self.innerlog.addline(mrespline)
      mrespline += "\n" # will be translated to \r\n
      mrespblock += mrespline

    self.clientio.write(mrespblock)

  #--------------------------------------------------------

  def run(self):

    while True:
      milliping(1)

      self.clientio.receive()

      mresplines = []
      mreq = self.clientio.getbymark("\n")
      if mreq:
        mreq = gcode_normalizereq(mreq)
        self.innerlog.addline(mreq)
        mresplines = self.marlincaller.call(mreq)

      if mresplines:
        self.reply(mresplines)

#==========================================================
