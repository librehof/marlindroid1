#!/usr/bin/env python3
#
# Application's entry point
#
# GPLv3 (C) 2021 librehof.com
#

from state import * # will import initial state
from app import *
from web import *
from webserver import *
from tcpremote import *
from serialremote import *

# ensure local directories
ensuredir("config")
ensuredir("log")
ensuredir("local")
ensuredir("data")
#ensuredir("rawdata")

from simple import *
from grbl2marlin import *
from marlin2marlin import *
from marlincaller import *

#==========================================================

# setup that can not fail (exit on failure)

def setup(service, dev):

  load_app_ini()

  APP.LOG.setup(APP.LOGFILE)

  app.note(f"{APP.LABEL} {APP.VERSION}")

  APP.SERVICE = service
  if APP.SERVICE:
    app.info("system service")

  if APP.DEV:
    app.info("dev mode")
  elif dev:
    APP.DEV = True
    app.info("dev override")

  app.threadstart("main")

  APP.WEBLOG = FileLog()
  APP.WEBLOG.setup(APP.WEBLOGFILE)

  webdispatcher = setupweb()
  webreloader = None
  if APP.DEV:
    webreloader = reloadweb
  APP.WEBSERVER = WebServer("web")
  APP.WEBSERVER.setup(APP.WEBNIC, APP.WEBPORT, webdispatcher, webreloader)

#==========================================================

def teardown():

  if APP.WEBSERVER:
    APP.WEBSERVER.teardown()

  if APP.WEBLOG:
    APP.WEBLOG.teardown()

  # we keep APP.LOG to the bitter end

  app.threadend()

#==========================================================

def run():

  loaded = False

  APP.CLIENTLOG = FileLog()
  APP.FIRMWARELOG = FileLog()
  APP.INNERLOG = FileLog()
  APP.STATUSLOG = FileLog()

  APP.GLISTENER = TcpListener("glistener")
  APP.GLISTENER.setup(APP.GNIC, APP.GPORT, 1, not APP.DEV)

  while not app.exit():

    APP.CLIENTIO = None
    APP.FIRMWAREIO = None
    APP.MARLINCALLER = None

    clienthandle = None
    firmwarehandle = None
    marlincaller = None

    MA.CLEAR()
    MA.SETSTATUS("Offline")
    WS.VALID = False

    try:
      milliping(20) # will throw AppExit

      load_machine_ini()
      load_workspace_file()
      loaded = True

      APP.CLIENTLOG.setup(APP.CLIENTLOGFILE)
      APP.FIRMWARELOG.setup(APP.FIRMWARELOGFILE)
      APP.INNERLOG.setup(APP.INNERLOGFILE)
      APP.STATUSLOG.setup(APP.STATUSLOGFILE)

      # Connect to firmware (machine)

      # send to marlin raw eol = '\n'
      # received raw eol = '\r\n'
      if APP.FIRMWAREIP:
        # => LAN => firmware
        app.note(f"Connecting to firmware on {APP.FIRMWAREIP}:{APP.FIRMWAREPORT}")
        firmwarehandle = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        firmwarehandle.connect((APP.FIRMWAREIP, APP.FIRMWAREPORT))
        APP.FIRMWAREIO = TcpRemote(firmwarehandle, "ascii", "\n", None, APP.FIRMWARELOG)
        firmwarehandle = None
        APP.FIRMWARECON = f"socket://{APP.FIRMWAREIP}:{APP.FIRMWAREPORT}"
      else:
        # => serial => firmware
        ttydevice=APP.FIRMWARETTY
        if ttydevice.startswith("/dev/") and ttydevice.endswith("*"):
          devicepattern = APP.FIRMWARETTY[:-1] + "*"
          devicefile = None
          for match in glob.glob(devicepattern):
            devicefile = match
            break
          if not devicefile:
            raise Exception(f"Could not find any {APP.FIRMWARETTY}")
          ttydevice = devicefile
        app.note(f"Connecting to firmware on {ttydevice}")
        firmwarehandle = newserial(ttydevice, APP.FIRMWAREBAUD)
        firmwarehandle.open()
        APP.FIRMWAREIO = SerialRemote(firmwarehandle, "ascii", "\n", None, APP.FIRMWARELOG)
        firmwarehandle = None
        APP.FIRMWARECON = f"{ttydevice} ({APP.FIRMWAREBAUD})"

      # Wait for gcode client to connect

      app.note(f"Waiting for gcode client to connect on {APP.GNIC}:{APP.GPORT}")
      while not clienthandle:
        milliping(20)
        clienthandle = APP.GLISTENER.accept()
      APP.CLIENTIO = TcpRemote(clienthandle, "ascii", "\r\n", None, APP.CLIENTLOG)
      clienthandle = None

      # Behaviour

      if APP.MODE != "simple":
        # extra translation layer
        marlincaller = MarlinCaller(APP.FIRMWAREIO)
        marlincaller.redefinepos(WS.WPOS, WS.EPOS)
      if APP.MODE == "simple":
        app.note(f"Client connected: {APP.CLIENTIO.address}")
        behaviour = Simple(APP.CLIENTIO, APP.FIRMWAREIO, APP.INNERLOG)
      elif APP.MODE == "grbl2marlin":
        app.note(f"GRBL client connected: {APP.CLIENTIO.address}")
        behaviour = Grbl2Marlin(APP.CLIENTIO, marlincaller, APP.INNERLOG)
      elif APP.MODE == "marlin2marlin":
        app.note(f"MARLIN client connected: {APP.CLIENTIO.address}")
        behaviour = Marlin2Marlin(APP.CLIENTIO, marlincaller, APP.INNERLOG)
      else:
        app.requestexit("SHUTDOWN")
        raise Exception(f"Unknown mode in server config: {APP.MODE}")
      behaviour.run()

    except AppExit:
      pass # will exit

    except Exception as e:
      # error and retry
      msg = estr(e, sys.exc_info()[2], APPROOT)
      app.error(msg)

    if loaded:
      try:
        # save state
        save_workspace_file()
      except AppExit:
        pass # will exit
      except Exception as e:
        # some serious error
        msg = estr(e, sys.exc_info()[2], APPROOT)
        app.error(msg)

    try:
      if APP.CLIENTIO is not None:
        APP.CLIENTIO.close()
        APP.CLIENTIO = None
      clienthandle = silentclose(clienthandle)
      if APP.FIRMWAREIO is not None:
        APP.FIRMWAREIO.close()
        APP.FIRMWAREIO = None
      firmwarehandle = silentclose(firmwarehandle)
      APP.CLIENTLOG.teardown()
      APP.FIRMWARELOG.teardown()
      APP.INNERLOG.teardown()
    except AppExit:
      pass # will exit
    except Exception as e:
      # error and retry
      msg = estr(e, sys.exc_info()[2], APPROOT)
      app.error(msg)

    if not app.exit():
      app.note("Will retry in 3 seconds...")
      longsleep(3)
      # will retry ...

  APP.GLISTENER.teardown()

#==========================================================

def main(args):

  try:
    service = False
    if "service" in args:
      service = True

    dev = False
    if "dev" in args:
      dev = True

    setup(service, dev)
    app.note("Running")

    # retry loop
    run()

  except AppExit:
    pass # we are exiting

  except Exception as e:
    # some serious error
    msg = estr(e, sys.exc_info()[2], APPROOT)
    app.error(msg)

  try:
    teardown()
  except Exception as e:
    # some serious error
    msg = estr(e, sys.exc_info()[2], APPROOT)
    app.error(msg)

  if not APP.EXITAT or not APP.EXITMETHOD:
    # make sure we will exit
    app.requestexit("ERROREXIT", 1)

#==========================================================

if __name__ == '__main__':

  main(sys.argv[1:])

#==========================================================
