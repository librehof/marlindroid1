#
# Grbl client <=> Marlin server (translation)
#
# GPLv3 (C) 2021 librehof.com
#

from app import *
from marlincaller import *

#==========================================================

#
# grbl notes:
#
#  grbl client uses \n eol when sending
#  grbl firmware responds with \r\n eol
#
#  status command '?' is sent without \n
#  => "<status>\r\n" without "ok\r\n"
#

# status examples
'''
< b'<Idle|MPos:0.000,0.000,0.000|FS:0,0|WCO:206.900,-49.400,-16.200>\r\r\n'
< b'<Idle|MPos:0.000,0.000,0.000|FS:0,0|Ov:100,100,100>\r\r\n'
< b'<Idle|MPos:0.000,0.000,0.000|FS:0,0>\r\r\n'
< b'<Idle|MPos:0.000,0.000,0.000|FS:0,0|WCO:206.900,-49.400,-16.200>\r\r\n'
< b'<Idle|MPos:0.000,0.000,0.000|FS:0,0|Ov:100,100,100>\r\r\n'
'''

#==========================================================

class Grbl2Marlin:

  def __init__(self, clientio, marlincaller, innerlog):

    self.clientio = clientio
    self.marlincaller = marlincaller
    self.innerlog = innerlog

    # gcode buffer
    # so we can catch and hold-back "real" gcode when entering "Hold"
    self.gcodefifo = queue.SimpleQueue()

  #--------------------------------------------------------

  # clear gcode cache

  def clearfifo(self):
    try:
      while not self.gcodefifo.empty():
        self.gcodefifo.get(False)
    except:
      pass

  #--------------------------------------------------------

  # get workspace offsets ($#)

  def workspacestate(self):

    self.marlincaller.refreshpos()

    gresplines = []
    gresplines.append(f"[G54:0,0,0]")
    if WS.GRID != 54:
      offset = WS.OFFSET(WS.GRID)
      negoffset= xyz_neg(offset)
      gresplines.append(f"[G{WS.GRID}:{xyz_str(negoffset)}]")
    gresplines.append("ok")

    return gresplines

  #--------------------------------------------------------

  # get grbl state ($G)

  def grblstate(self):

    self.marlincaller.refreshpos()
    gresp = f"[GC:G0 G{WS.GRID} G17 G21 G90 G94 M5 M9 T0 F0 S0]"
    return [gresp,"ok"]

  #--------------------------------------------------------

  # grlb kill alarm ($X)

  def grblkillalarm(self):

    self.clearfifo()
    self.marlincaller.resetstate()
    return ["ok"]

  #--------------------------------------------------------

  # grbl jog ($J)

  def grbljog(self, greq):

    self.marlincaller.refreshpos()

    kv = greq.split("=")
    if len(kv) != 2:
      return ["ok"]

    relative = kv[1]
    if not relative.startswith("G91"):
      return ["ok"]

    fields = relative.split(" ")
    x = WS.WPOS[0]
    y = WS.WPOS[1]
    z = WS.WPOS[2]
    a = WS.EPOS[0]
    isa = False
    b = WS.EPOS[1]
    isb = False
    c = WS.EPOS[2]
    isc = False
    f = None
    for field in fields:
      if field.startswith("X"):
        x += gcode_lettervalue(field)
      elif field.startswith("Y"):
        y += gcode_lettervalue(field)
      elif field.startswith("Z"):
        z += gcode_lettervalue(field)
      elif field.startswith("A"):
        a += gcode_lettervalue(field)
        isa = True
      elif field.startswith("B"):
        b += gcode_lettervalue(field)
        isb = True
      elif field.startswith("C"):
        c += gcode_lettervalue(field)
        isc = True
      elif field.startswith("F"):
        f = gcode_lettervalue(field)

    if f is not None and MA.JOGMAX is not None:
      if f > MA.JOGMAX:
        f = MA.JOGMAX # limit

    mreq = f"G0 X{x:.3f} Y{y:.3f} Z{z:.3f}"

    if not isa and not isb and not isc:
      if f:
        mreq += f" F{f:.0f}"
      return self.marlincaller.rawcall(mreq, self.preprocess)
    else:
      if isa and MA.AAXIS:
        mreq += f" E{a:.3f}"
        if f:
          mreq += f" F{f:.0f}"
        self.marlincaller.rawcall(mreq)
      if isb and MA.BAXIS:
        # TODO
        pass
      if isc and MA.CAXIS:
        # TODO
        pass
      return ["ok"]

  #--------------------------------------------------------

  def grbl2marlincall(self, greq):

    # special grbl commands
    if greq == "$#":
      return self.workspacestate()
    elif greq == "$X":
      return self.grblkillalarm()
    elif greq == "$G":
      return self.grblstate()
    elif greq == "$H":
      return self.marlincaller.call("G28", self.preprocess)
    elif greq.startswith("$J="):
      return self.grbljog(greq)
    elif greq.startswith("G43.1 Z0.0"):
      # TODO: Dynamic Tool Length Offset??
      return ["ok"] # do nothing for now

    mresplines = self.marlincaller.call(greq, self.preprocess)
    return mresplines

  #--------------------------------------------------------

  def grblreply(self, gresplines):

    grespblock = ""
    for grespline in gresplines:
      if not grespline.startswith("<"):
        self.innerlog.addline(grespline)
      grespline += "\n" # will be translated to \r\n
      grespblock += grespline

    self.clientio += grespblock

  #--------------------------------------------------------

  # "!!"

  def grblstop(self):

    if MA.STATUS in ("Hold"):
      self.clearfifo()
      self.marlincaller.resetstate()
    elif MA.STATUS not in ("Idle"):
      app.note("Control: Quick stop!")
      mreq = f"M410" # quick stop
      self.marlincaller.rawcall(mreq)
    else:
      app.note("Control: Quick stop in Idle (ignoring)")

  #--------------------------------------------------------

  # "!"

  def grblhold(self):

    if MA.STATUS in ("Run","Jog"):
      app.note("Control: Hold")
      MA.SETSTATUS("Hold")
    else:
      app.note("Control: Hold without moving (ignoring)")

  #--------------------------------------------------------

  # "~"

  def grblresume(self):

    if MA.STATUS == "Hold":
      app.note("Control: Resume")
      MA.SETSTATUS("Run")
      MA.HOLD = False
    else:
      app.note("Control: Resume without hold status (ignoring)")

  #--------------------------------------------------------

  # execute side-channel commands, like emergency stop

  def preprocess(self, emergencycheck=False):

    self.clientio.receive()

    # "?" status request(s)
    gstatus = False
    while self.clientio.getmark("?"):
      gstatus = True # group multiple status requests into one
    if gstatus:
      # send back side-channel status
      if not emergencycheck:
        self.marlincaller.refreshpos() # this will work during "hold" to (M gcode)
      mpos = WS.MPOS()
      wco = WS.WCO()
      gresp = f"<{MA.STATUS}|MPos:{xyz_str(mpos)},{xyz_str(WS.EPOS)}|WCO:{xyz_str(wco)}>"
      self.grblreply([gresp]) # without ok

    # "0x18" reset
    if self.clientio.getmark(chr(0x18)):
      if not emergencycheck and MA.STATUS not in ("Run","Jog"):
        self.clearfifo()
        self.marlincaller.resetstate()
      else:
        app.note("Control: Reset while busy (try Hold or Stop)")

    # "!" stop/hold
    if self.clientio.getmark("!!"):
      if not emergencycheck:
        self.marlincaller.refreshpos()
      self.grblstop()
    elif self.clientio.getmark("!"):
      millisleep(250)
      self.clientio.receive()
      if self.clientio.getmark("!"):
        if not emergencycheck:
          self.marlincaller.refreshpos()
        self.grblstop()
      else:
        self.grblhold()

    # "~" resume
    if self.clientio.getmark("~"):
      self.grblresume()

    # put real gcode in fifo (for later execution)
    greq = self.clientio.getbymark("\n")
    if greq:
      gnormreq = gcode_normalizereq(greq)
      self.gcodefifo.put(gnormreq)

  #--------------------------------------------------------

  def run(self):

    while True:
      milliping(1)

      self.preprocess()
      if self.gcodefifo.empty():
        continue # fifo is empty

      if MA.HOLD:
        self.marlincaller.refreshpos()
        continue # hold back fifo gcode

      # process one gcode
      gnormreq = self.gcodefifo.get(False)
      self.innerlog.addline(gnormreq)
      gresplines = self.grbl2marlincall(gnormreq)
      if gresplines:
        self.grblreply(gresplines)

#==========================================================
