#
# Web server doing http encoding/decoding and invoking dispatcher
#
# dispatcher interface:
#   dispatcher.initiate(remote) # on new call
#   dispatcher.dispatch(remote) # if delegated=False
#   dispatcher.close() # take down any thread pool
#   HyperNotFound, HyperNotModified, Hyper<some>Redirect
#   any other Exception => "500 Server Error"
#
# TODO: advanced features
# APP.WEBHOURLYMAXMB => temporarily block ip when exceeding
# APP.WEBGZIPMINKB => gzip text (with zlib) when equal or above
# ip startswith whitelist
#

from app import *
from webremote import *

#==========================================================

# web server (taking url dispatcher)

# single worker thread, dispatcher may use thread pool

class WebServer(TcpListener):

  # construct (disabled)

  def __init__(self, label):

    super().__init__(label)

    self.dispatcher = None
    self.reloader = None

    # internal
    self.thread = None
    self.stopped = threading.Event()
    self.stopped.set()
    self.remotes = set() # active web connections

  #--------------------------------------------------------

  def setup(self, nic, port, dispatcher, reloader):

    self.teardown()

    super().setup(nic, port, APP.WEBMAXOPEN, APP.WEBCHECKPORT)

    self.dispatcher = dispatcher
    self.reloader = reloader

    if not self.enabled():
      return # disabled

    # start web thread
    self.stopped.clear() # thread will set on return
    self.thread = threading.Thread(target=self.run)
    self.thread.start()

  #--------------------------------------------------------

  # teardown (disabled until setup)

  def teardown(self):

    if not self.enabled():
      return # disabled

    # signal stop
    self.thread = None

    # wait for stop
    timeout = app.timeout() * 2
    self.stopped.wait(timeout)
    if not self.stopped.isSet():
      raise Exception("Could not stop {self.label} thread")

    super().teardown()

  #--------------------------------------------------------

  # log helper

  def info(self, msg, address=None):

    if not APP.WEBLOG:
      return

    if address:
      msg = f"[{address}] {msg}"

    APP.WEBLOG.addline(msg)

  #--------------------------------------------------------

  # log helper

  def error(self, msg, address=None):

    if address:
      msg = f"[{address}] {msg}"

    if not address:
      app.error(msg) # outer error
    else:
      app.debug(msg) # inner error (low prio)

    if APP.WEBLOG:
      msg = f"ERROR: {msg}"
      APP.WEBLOG.addline(msg)

  #--------------------------------------------------------

  # helper: disconnect and remove remote

  def remove(self, remote):

    if not remote in self.remotes:
      return

    remote.close()
    self.remotes.remove(remote)

    self.info("disconnect", remote.address)

  #--------------------------------------------------------

  # helper

  def doexception(self, remote):

    if isinstance(remote.exception, StreamClose) and not remote.activecall():
      # valid disconnect request
      self.remove(remote) # disconnect
      return

    if remote.resp:
      # special case: late exception can not be sent back
      self.error(remote.message, remote.address)
      self.remove(remote) # disconnect
      return

    elif isinstance(remote.exception, HyperRedirect):
      # redirect
      try:
        resp = HyperResponse(remote.exception.statuscode, remote.exception.statusphrase, "text/plain", "utf-8")
        resp.header["location"] = str(remote.exception)
        remote.sendresponse(resp) # send!
        self.info(repr(remote.resp), remote.address)
        remote.clearcall() # reuse
      except Exception as e:
        msg = estr(e, sys.exc_info()[2], APPROOT)
        self.error(msg)
        self.remove(remote) # disconnect

    elif isinstance(remote.exception, HyperException):
      # not found, not modified, ...
      try:
        resp = HyperResponse(remote.exception.statuscode, remote.exception.statusphrase, "text/plain", "utf-8")
        content = str(remote.exception)
        rawcontent = makeutf8(content)
        remote.sendresponse(resp, rawcontent) # send!
        self.info(repr(remote.resp), remote.address)
        remote.clearcall() # reuse
      except Exception as e:
        msg = estr(e, sys.exc_info()[2], APPROOT)
        self.error(msg)
        self.remove(remote) # disconnect

    else:
      # server error
      self.error(remote.message, remote.address)
      try:
        resp = HyperResponse(500, "Server Error", "text/plain", "utf-8")
        rawcontent = makeutf8(remote.message)
        remote.sendresponse(resp, rawcontent) # send!
        self.info(repr(remote.resp), remote.address)
      except Exception as e:
        msg = estr(e, sys.exc_info()[2], APPROOT)
        self.error(msg)
      finally:
        self.remove(remote) # disconnect

  #--------------------------------------------------------

  # helper

  def doheader(self, remote, rawheader):

    # connection keep alive time
    idle = APP.WEBMAXOPEN - len(self.remotes)
    if app.timeout() >= 3 and idle > APP.WEBMAXOPEN//2:
      maxkeep = (app.timeout()*3)//4
    else:
      maxkeep = None

    remote.newcall(rawheader, maxkeep)
    self.info(repr(remote.req), remote.address)

    self.dispatcher.initiate(remote) # initiate!

  #--------------------------------------------------------

  # helper

  def doremote(self, remote):

    try:
      if remote.exception:
        self.doexception(remote) # exception!
      if not remote.req:
        # receive (looking for request header)
        if remote.receive():
          # try pop request header
          rawheader = remote.getbymark(b'\r\n\r\n')
          if rawheader:
            self.doheader(remote, rawheader)
      if remote.req:
        # has been passed on to dispatcher
        if remote.responded:
          # responded!
          self.info(repr(remote.resp), remote.address)
          remote.clearcall()
        elif remote.delegated:
          # delegated...
          millisleep(1)
        else:
          # inline...
          self.dispatcher.dispatch(remote)

    except Exception as e:
      remote.setexception(e, sys.exc_info()[2])

  #--------------------------------------------------------

  def run(self):

    app.threadstart(self.label)

    # loop until teardown or exit
    while self.thread:
      try:
        if APP.WEBMAXOPEN > 10:
          milliping(1)
        else:
          milliping(5)

        if self.reloader:
          newdispatcher = self.reloader()
          if newdispatcher:
            self.dispatcher = newdispatcher
            app.note("WEB RELOADED!")
            APP.WEBRELOADED = True
            millisleep(5)

        # accept new
        handle = self.accept()
        if handle:
          # add remote!
          remote = WebRemote(handle)
          self.remotes.add(remote)
          self.info(f"connected", remote.address)

        # process connected remotes
        for remote in list(self.remotes):
          self.doremote(remote)

      except AppExit:
        break # end

      except Exception as e:
        # outer error (no specific remote)
        msg = estr(e, sys.exc_info()[2], APPROOT)
        self.error(msg)
        millisleep(10)

    # disconnect all remotes
    try:
      self.dispatcher.close()
      for remote in list(self.remotes):
        millisleep(1)
        self.remove(remote)
    except Exception as e:
      # outer error
      msg = estr(e, sys.exc_info()[2], APPROOT)
      self.error(msg)

    self.stopped.set()
    app.threadend()

#==========================================================
