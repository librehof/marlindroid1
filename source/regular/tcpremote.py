#
# Remote communication using TCP socket
#

import socket

from app import *
from remote import *

#==========================================================

# tcp stream with buffer (optional text encoding)

class TcpRemote(Remote):

  def __init__(self, handle, charset=None, newline=None, maxinactive=None, log=None):

    super().__init__(handle, charset, newline, maxinactive, log)

    # remote address, usually ipv4 like "n.n.n.n"
    peer = handle.getpeername()
    if len(peer) == 2:
      self.address = peer[0]
    else:
      self.address = str(peer)

    # make non-blocking
    #fcntl.fcntl(handle, fcntl.F_SETFL, os.O_NONBLOCK)
    handle.setblocking(0)

  #--------------------------------------------------------

  # low-level read (non-blocking)

  # returns: raw part or None if no data
  # will raise StreamClose() on disconnect
  # will raise RemoteInactive if maxinactive is reached

  def read(self):

    try:
      rawpart = self.handle.recv(128*1024)
    except socket.timeout:
      return self.nodata()
    except socket.error as e:
      status = e.args[0]
      if status == errno.EAGAIN or status == errno.EWOULDBLOCK:
        return self.nodata()
      else:
        raise e # real error

    if not rawpart:
      raise StreamClose(f"Disconnected from {self.address}")

    if self.log:
      line = f"> {str(rawpart)}"
      self.log.addline(line)

    self.lastactive = APP.TICK
    return rawpart

  #--------------------------------------------------------

  # low-level write

  def write(self, rawpart):

    if not rawpart:
      return

    if self.log:
      line = f"< {str(rawpart)}"
      self.log.addline(line)

    self.handle.sendall(rawpart)
    self.lastactive = APP.TICK

#==========================================================

# listening for incoming tcp socket connection(s) on nic:port

class TcpListener():

  # construct (disabled)

  def __init__(self, label=None):

    if not label:
      label = "listener"

    self.label = label

    self.nic = None # network interface (card)
    self.port = None
    self.maxopen = None
    self.checkport = False

    self.handle = None

  #--------------------------------------------------------

  # setup (enabled or disabled depending on config)

  # maxopen: max active connections
  # checkport: check port is not in use

  def setup(self, nic=None, port=8000, maxopen=10, checkport=True):

    self.teardown()

    if nic == "":
      app.warning("Blank NIC (use None)")
      nic = None

    self.nic = nic
    self.port = port
    self.maxopen = maxopen
    self.checkport = checkport

    if not nic:
      return # disabled

    if checkport:
      submsg = f"listening on {self.nic}:{self.port}"
    else:
      submsg = f"listening on shared {self.nic}:{self.port}"
    app.note(f"Enabling {self.label} ({submsg})")

    self.handle = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    self.handle.settimeout(0) # non-blocking
    self.handle.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack('ii',1,0)) # hard server close
    if not checkport:
      # don't care if in use (or not properly closed)
      self.handle.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    slot = (self.nic, self.port)
    self.handle.bind(slot)
    self.handle.listen(self.maxopen)
    millisleep(10)

    return # success

  #--------------------------------------------------------

  # teardown (disabled until setup)

  def teardown(self):

    if not self.handle:
      return

    app.note(f"Disabling {self.label} ({self.nic}:{self.port})")

    try:
      self.handle.close()
    except Exception as e:
      msg = estr(e, sys.exc_info()[2], APPROOT)
      app.error(msg)
    finally:
      self.handle = None

    self.nic = None
    millisleep(10)

  #--------------------------------------------------------

  # enabled or not (by config)

  def enabled(self):

    if self.nic:
      return True
    else:
      return False

  #----------------------------------------------------------

  # check for, and accept, first available TCP connection request

  # returns socket if new connection was established
  # returns None otherwise (non-blocking)

  def accept(self):

    if not self.handle:
      return None

    try:
      remotehandle,address = self.handle.accept()
      return remotehandle

    except socket.timeout:
      return None # no connection

    except socket.error as e:
      status = e.args[0]
      if status == errno.EAGAIN or status == errno.EWOULDBLOCK:
        return None # no connection
      else:
        raise e # error

  #--------------------------------------------------------

  def __str__(self):

    return f"nic({self.nic}) port({self.port})"

  #--------------------------------------------------------

  def __repr__(self):

    return str(self)

#==========================================================
