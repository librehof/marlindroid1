#
# Web dispatcher (server-threaded)
#
# TODO: thread pool if APP.WEBPOOL
#

from app import *
from webremote import *

#==========================================================

# make utf content ready to be sent with correct charset
# returns (raw, charset)

def webutf(raw, ext):

  bom = checkbom(raw)
  if bom == codecs.BOM_UTF16_LE:
    charset = "utf-16"
  elif bom == codecs.BOM_UTF16_BE:
    charset = "utf-16"
  elif bom == codecs.BOM_UTF8:
    charset = "utf-8" # utf-8 with bom
    raw = raw.replace(b'\r', b'')
  else:
    charset = "utf-8" # default to utf-8 without bom
    raw = raw.replace(b'\r', b'')
    if ext == ".var": # single line
      raw = raw.replace(b'\n', b'')
  return (raw, charset)

#==========================================================

class WebDispatcher:

  def __init__(self, media, constant, dynamic, endpoints):

    self.media = media         # ( ("/url","dir") )
    self.constant = constant   # ( ("/url","file"[,"altext"]) )
    self.dynamic = dynamic     # { "/url": entry }
    self.endpoints = endpoints # { "endpath": entry }

    self.cache = { } # { "url": (content, mediatype, charset, md5) }

    # cache constant content
    for entry in constant:
      url = entry[0]
      path = entry[1]
      if len(entry) > 2:
        ext = entry[2]
      else:
        ext = pathext(path)
      raw = loadraw(path)
      self.addconstant(url, raw, ext)

  #--------------------------------------------------------

  def addconstant(self, url, raw, ext):

    # mediatype and charset
    if ext in APP.UTFEXTS:
      mediatype = APP.UTFEXTS[ext]
      raw,charset = webutf(raw,ext)
    elif ext in APP.RAWEXTS:
      mediatype = APP.RAWEXTS[ext]
      charset = None
    else:
      raise Exception(f"Extension not supported ({ext})")

    # hash
    md5 = rawmd5(raw)

    # add
    entry = (raw, mediatype, charset, md5)
    self.cache[url] = entry

  #--------------------------------------------------------

  # helper: returns path or raises exception

  def checkmedia(self, mediadir, suburl):

    # safety check (!)
    parts = suburl.split("/")
    for part in parts:
      if not textislabel(part,True):
        raise Exception(f"Illegal media filename: /{mediadir}/{suburl}")

    subpath = suburl.replace("/", os.pathsep)
    path = pathjoin(mediadir, subpath)
    if not pathisfile(path):
      raise HyperNotFound(f"/media/{suburl}")

    return path

  #--------------------------------------------------------

  # helper

  def sendmedia(self, remote, resp, path):

    # construct weak etag (quick)
    size = filesize(path)
    lastutc = filemodified(path)
    filename = pathleaf(path)
    unique = f"{filename}.{size}.{lastutc}"
    md5 = textmd5(unique)
    etag = f'W/"{md5}"' # W/ = weak (etag not from content)

    match = remote.req.header.get("if-none-match")
    if match and match == etag:
      raise HyperNotModified(remote.req.url)

    if APP.DEV:
      maxage = 1
    else:
      maxage = 5
    resp.header["cache-control"] = f"max-age={maxage},must-revalidate"
    resp.header["etag"] = etag

    # TODO: stream in chunks to save on RAM usage
    rawo = loadraw(path)

    ext = pathext(path)
    if ext in APP.RAWEXTS:
      # raw
      resp.mediatype = APP.RAWEXTS[ext]
      resp.charset = None
    elif ext in APP.UTFEXTS:
      # text
      resp.mediatype = APP.UTFEXTS[ext]
      rawo,resp.charset = webutf(rawo,ext)
    else:
      # unknown type
      resp.mediatype = "application/octet-stream"
      resp.charset = None

    remote.sendresponse(resp, rawo)

  #--------------------------------------------------------

  # helper

  def sendcached(self, remote, resp, rawo, mediatype, charset, md5):

    etag = f'"{md5}"'

    match = remote.req.header.get("if-none-match")
    if match and match == etag:
      raise HyperNotModified(remote.req.url)

    if not mediatype:
      # unknown type
      resp.mediatype = "application/octet-stream"
      resp.charset = None
    else:
      resp.mediatype = mediatype
      resp.charset = charset

    if APP.DEV:
      maxage = 1
    else:
      maxage = 5
    resp.header["cache-control"] = f"max-age={maxage},must-revalidate"
    resp.header["etag"] = f'"{md5}"'

    remote.sendresponse(resp, rawo)

  #--------------------------------------------------------

  # helper: rawi => anyi

  # None => None
  # "[*:]raw" => bytes
  # "[*:]text" => str
  # "form:map" => dict
  # "json:map" => dict
  # "tab:list" => list

  def anyi(self, reqtype, req, rawi):

    if not reqtype:
      return None # ignore content

    elif reqtype == "raw" or reqtype.endswith(":raw"):
      return rawi # raw

    elif reqtype == "text" or reqtype.endswith(":text"):
      texti = rawtotext(rawi, req.charset)
      return texti # text

    elif reqtype == "form:map":
      if not rawi:
        return { }
      if req.mediatype == "application/x-www-form-urlencoded":
        # TODO: use charset if given? look at https://tools.ietf.org/html/rfc3986
        texti = rawi.decode("utf-8")
        formi = spliturlvars(texti) # urlencoded
        return formi
      else:
        raise Exception(f"Expected form data, got {req.mediatype}")

    elif reqtype == "json:map":
      texti = rawtotext(rawi, req.charset)
      if not texti:
        return { } # empty dict
      else:
        return json.loads(texti) # json dict

    elif reqtype == "tab:list":
      raise Exception("TODO: tab not supported yet") # TODO

    else:
      raise Exception(f"Internal: Unsupported web request type: {reqtype}")

  #--------------------------------------------------------

  # helper: initial anyo

  # None => None
  # "raw" => Raw (application/octet-stream)
  # "text" => Text (text/plain)
  # "html:text" => HtmlText (text/html)
  # "xml:text" => XmlText (text/xml)
  # "js:text" => JsonText (text/javascript)
  # "json:text" => JsonText (application/json)
  # "json:map" => dict (application/json)
  # "tab:list" => list (text/tab-separated-values)
  # ".ext:text" => Text (ext=>mediatype)
  # ".ext:raw" => Raw (ext=>mediatype)
  # ".ext:textstream" => WebStream(text) (ext=>mediatype)
  # ".ext:rawstream" => WebStream(raw) (ext=>mediatype)

  def anyo(self, resptype, resp, handle):

    if not resptype:
      return None

    elif resptype == "raw":
      resp.mediatype = "application/octet-stream"
      return Raw()

    elif resptype == "text":
      resp.mediatype = "text/plain"
      resp.charset = "utf-8" # default
      return Text()

    elif resptype == "html:text":
      resp.mediatype = "text/html"
      resp.charset = "utf-8" # default
      return MarkupText()

    elif resptype == "xml:text":
      resp.mediatype = "text/xml"
      resp.charset = "utf-8" # default
      return MarkupText()

    elif resptype == "js:text":
      resp.mediatype = "text/javascript"
      resp.charset = "utf-8" # default
      return JsonText()

    elif resptype == "json:text":
      resp.mediatype = "application/json"
      resp.charset = "utf-8" # default
      return JsonText()

    elif resptype == "json:map":
      resp.mediatype = "application/json"
      resp.charset = "utf-8" # default
      return { }

    elif resptype == "tab:list":
      resp.mediatype = "text/tab-separated-values"
      resp.charset = "utf-8" # default
      return [ ]

    elif resptype.startswith(".") and resptype.endswith(":text"):
      ext = resptype[:-len(":text")]
      resp.mediatype = APP.UTFEXTS.get(ext)
      if not resp.mediatype:
        raise Exception(f"Internal: Missing mediatype for {ext} text")
      resp.charset = "utf-8" # default
      return Text()

    elif resptype.startswith(".") and resptype.endswith(":raw"):
      ext = resptype[:-len(":raw")]
      resp.mediatype = APP.RAWEXTS.get(ext)
      if not resp.mediatype:
        raise Exception(f"Internal: Missing mediatype for {ext} raw")
      return Raw()

    elif resptype.startswith(".") and resptype.endswith(":textstream"):
      ext = resptype[:-len(":textstream")]
      resp.mediatype = APP.UTFEXTS.get(ext)
      if not resp.mediatype:
        raise Exception(f"Internal: Missing mediatype for {ext} text")
      resp.charset = "utf-8" # default
      return WebStream(handle, resp.charset) # charset updated after precall

    elif resptype.startswith(".") and resptype.endswith(":rawstream"):
      ext = resptype[:-len(":rawstream")]
      resp.mediatype = APP.RAWEXTS.get(ext)
      if not resp.mediatype:
        raise Exception(f"Internal: Missing mediatype for {ext} raw")
      return WebStream(handle)

    else:
      raise Exception(f"Internal: Unsupported web response type: {resptype}")

 #--------------------------------------------------------

  # helper: anyo => rawo

  def rawo(self, resptype, resp, anyo):

    if not resptype:
      return None

    elif resptype == "raw" or resptype.endswith(":raw"):
      return bytes(anyo)

    elif resptype == "text" or resptype.endswith(":text"):
      texto = str(anyo)

    elif resptype == "json:map":
      texto = json.dumps(anyo, indent=2, sort_keys=False)

    elif resptype == "tab:list":
      text = Text()
      for line in anyo:
        if isinstance(line, str):
          text += f"{line}\n"
        else:
          text += "\t".join(line) + "\n"
      texto = str(text)

    else:
      raise Exception(f"Internal: Unsupported web response type: {resptype}")

    # text to raw

    if not resp.charset:
      resp.charset = "utf-8"

    rawo = makeraw(texto)
    return rawo

  #--------------------------------------------------------

  def initiate(self, remote):

    # check
    if remote.req.compression:
      raise Exception(f"Compressed HTTP request not supported ({remote.req.compression})") # TODO
    elif remote.req.chunked:
      raise Exception("Chunked HTTP request not supported") # TODO
    elif remote.req.mediatype == "multipart/form-data":
      remote.formdata = { } # receive multipart here
    elif remote.req.contentlength is None:
      raise Exception("HTTP request without content length")

  #--------------------------------------------------------

  def dispatch(self, remote):

    remote.receive()

    rawi = None
    formi = None

    # receive request content

    if remote.formdata is None:
      # content by length
      length = remote.req.contentlength
      remote.formdata = remote.getbylength(length)
      if remote.formdata is None:
        return # we need more!
    else:
      # content by form header and boundary (multipart)
      if not remote.formheader:
        rawheader = remote.getbymark(b'\r\n\r\n')
        if rawheader is None:
          return # we need more form header!
        remote.formheader = rawheader.decode("iso-8859-1")
        return  # we need data!
      else:
        rawdata = remote.getbymark(remote.req.rawmark)
        if rawdata is None:
          return # we need more form data!
        if remote.getmark(b'\r\n'):
          end = False
        elif remote.getmark(b'--\r\n'):
          end = True
        else:
          raise Exception(f"Unexpected multipart boundary ending: {remote.req.url}")
        lines = remote.formheader.split("\r\n")
        disposition = hypercontentdisposition(lines)
        if not disposition:
          raise Exception(f"Missing multipart disposition: {remote.req.url}")
        name = disposition[0]
        filename = disposition[1]
        if not name:
          raise Exception(f"Missing multipart disposition name: {remote.req.url}")
        if filename:
          # raw file
          key = f"{name}:filename"
          remote.formdata[key] = filename
          remote.formdata[name] = rawdata
        else:
          # text value
          textvalue = rawtotext(rawdata, remote.req.charset) # TODO: verify charset handling
          remote.formdata[name] = textvalue
        if not end:
          remote.formheader = None
          return # we need more!

    # full request received

    url = remote.req.url
    if not url:
      raise Exception("Unknown URL")

    resp = HyperResponse(200, "OK")

    # media file
    for entry in self.media:
      mediabase = entry[0] + "/"
      mediadir = entry[1]
      if url.startswith(mediabase):
        suburl = url[len(mediabase):]
        path = self.checkmedia(mediadir, suburl)
        self.sendmedia(remote, resp, path)
        return # done!

    # cached content (constant)
    entry = self.cache.get(url)
    if entry:
      self.sendcached(remote, resp, entry[0], entry[1], entry[2], entry[3])
      return # done!

    # check if endpoint
    remote.req.name = None
    remote.req.apirev = None
    if url.startswith("/api"): # url = /api<ver>/<endpath>
      revname = url[4:]
      pos = revname.find("/")
      if pos >= 0:
        # is endpoint request
        remote.req.name = revname[pos+1:]
        revstr = revname[:pos]
        if revstr:
          remote.req.apirev = textasint(revstr)
          if remote.req.apirev is None or remote.req.apirev < 1:
            raise Exception(f"Illegal API revision, expected positive integer ({revstr})")

    entry = None
    if remote.req.name:
      # try lookup api endpoint (machine request)
      entry = self.endpoints.get(remote.req.name)
    if not entry:
      # lookup dynamic content (browser request)
      longentry = self.dynamic.get(url)
      if longentry:
        # convert to short entry
        if len(longentry) < 4:
          raise Exception(f"Internal: Missing fields in dynamic entry: {url}")
        remote.req.name = longentry[0]
        entry = longentry[1:]

    if not entry:
      raise HyperNotFound(f"Not found: {url}") # not found!

    # exception entry?
    if isinstance(entry,Exception):
      raise entry # http exception!

    entrylen = len(entry)
    if entrylen < 3:
      raise Exception(f"Internal: Missing fields in entry: {url}")

    reqtype = entry[0]
    resptype = entry[1]
    precall = entry[2]

    if entrylen == 3 and not precall:
      raise Exception(f"Internal: Expected precall entry: {url}")

    # rawi => anyi
    if remote.req.mediatype == "multipart/form-data":
      anyi = remote.formdata
    else:
      anyi = self.anyi(reqtype, remote.req, remote.formdata)

    anyo = self.anyo(resptype, resp, remote.handle)

    if precall:
      precall(reqtype, resptype, remote.req, resp)

    if isinstance(anyo,WebStream):
      # start of stream
      resp.chunked = True
      resp.contentlength = None
      anyo.charset = resp.charset # final charset
      remote.sendheader(resp)

    for call in entry[3:]:
      call(remote.req, anyi, anyo)

    if isinstance(anyo,WebStream):
      # end of stream
      rawend = b"0\r\n\r\n"
      remote.sendcontent(rawend)
    else:
      # anyo => rawo
      rawo = self.rawo(resptype, resp, anyo)
      remote.sendresponse(resp, rawo)

  #--------------------------------------------------------

  def close(self):

    pass # TODO: close thread pool if configured

#==========================================================
