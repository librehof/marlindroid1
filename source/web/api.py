#
# api.py * GPLv3 (C) 2021 librehof.com
#

from web import *
from app import *

#========================================================

def header(reqtype, resptype, req, resp):

  pass

#========================================================

def now(req, nonei, mapo):

  nowdt = hostnowdt()
  mapo["day"] = nowdt.strftime("%Y-%m-%d")
  mapo["time"] = nowdt.strftime("%H:%M:%S")

#========================================================

def pos(req, nonei, mapo):

  mapo["grid"] = WS.GRID
  mapo["x"] = WS.WPOS[0]
  mapo["y"] = WS.WPOS[1]
  mapo["z"] = WS.WPOS[2]
  mapo["a"] = WS.EPOS[0]

#========================================================
