#
# marlindroid1 web structure
#
# GPLv3 (C) 2021 librehof.com
#

import importlib

from app import *
from webdispatcher import *

# web
import api
import frame
import statustab
import tabletab
import infotab

#==========================================================

# called by main during setup
# returns webdispatcher

def setupweb():

  # global
  APP.UTFEXTS = {
    ".txt": "text/plain",
    ".var": "text/plain",
    ".list": "text/plain",
    ".tab": "text/tab-separated-values",
    ".html": "text/html",
    ".css": "text/css",
    ".js": "text/javascript",
    ".json": "application/json",
    ".xml": "text/xml",
    ".svg": "image/svg+xml",
  }

  # global
  APP.RAWEXTS = {
    ".jpg": "image/jpeg",
    ".png": "image/png",
    ".ico": "image/x-icon",
  }

  media = [
    ("/media", "media"), # url=/media/<file>
  ]

  constant = [
    ("/label", "meta/label.var"),
    ("/version", "meta/version.var"),
    ("/repoid", "meta/repoid.var"),
    ("/favicon", "meta/icon64.png"),
    ("/icon", "meta/icon256.png"),
    ("/script.js", APPSOURCE+"/web/script.js"),
    ("/style.css", APPSOURCE+"/web/style.css"),
  ]

  dynamic = {
    "/index.html": HyperPermanentRedirect("/"),
    "/status": HyperPermanentRedirect("/"),
    "/": ("status", "form:map", "html:text", frame.header, frame.top, statustab.body, frame.bottom),
    "/table": ("table", "form:map", "html:text", frame.header, frame.top, tabletab.body, frame.bottom),
    "/info": ("info", "form:map", "html:text", frame.header, frame.top, infotab.body, frame.bottom),
    "/poll": ("poll", "json:map", "js:text", frame.header, frame.poll),
  }

  endpoints = { # url = /api<ver>/[<subapi>/]<endpoint>
    "now": (None, "json:map", api.header, api.now),
    "pos": (None, "json:map", api.header, api.pos),
  }

  return WebDispatcher(media, constant, dynamic, endpoints)

#==========================================================

# called by webserver (in dev mode)
# returns new webdispatcher or none if not reloaded

class WEBDIR:

  FILETIMES = { } # path = mtime

def reloadweb():

  if not WEBDIR.FILETIMES:
    # cache last modified
    _,_,filenames = next(os.walk(f"{APPSOURCE}/web"))
    for filename in filenames:
      filepath = f"{APPSOURCE}/web/{filename}"
      filetime = filemodified(filepath)
      WEBDIR.FILETIMES[filepath] = filetime

  changed = False
  for filepath,filetime in WEBDIR.FILETIMES.items():
    newtime = filemodified(filepath)
    if newtime != filetime:
      changed = True
      WEBDIR.FILETIMES[filepath] = newtime

  if not changed:
    return # done

  # reload!

  import api
  importlib.reload(api)

  import frame
  importlib.reload(frame)

  import statustab
  importlib.reload(statustab)

  import infotab
  importlib.reload(infotab)

  return setupweb()

#==========================================================
