#
# frame.py * GPLv3 (C) 2021 librehof.com
#

from web import *

#========================================================

# TODO: include tail of status log

def body(req, formi, texto):

  machineinfo = MA.MACHINEINFO()
  firmwareinfo = MA.FIRMWAREINFO()

  clientcon = f"socket://{APP.GNIC}:{APP.GPORT}"

  statustail = []
  if APP.STATUSLOGFILE and pathexists(APP.STATUSLOGFILE):
    statuslog = loadutf8(APP.STATUSLOGFILE)
    statuslines = statuslog.split("\n")
    count = 0
    pos = len(statuslines) - 1
    while pos >= 0 and count < 20:
      line = statuslines[pos]
      if line:
        statustail.append(statuslines[pos])
      count += 1
      pos -= 1

  # header table

  texto += \
f'''\
<table style="padding-left:4px; padding-right:4px; padding-bottom:6px;">
<tr><td>{APP.TITLE}:</td><td>{APP.VERSION}</td></tr>
<tr><td></td><td></td></tr>
<tr><td>MACHINEINFO:</td><td>{machineinfo}</td></tr>
<tr><td>FIRMWAREINFO:</td><td>{firmwareinfo}</td></tr>
<tr><td></td><td></td></tr>
<tr><td>CLIENTCON:</td><td>{clientcon}</td></tr>
<tr><td>FIRMWARECON:</td><td>{APP.FIRMWARECON}</td></tr>
<tr><td></td><td></td></tr>
<tr><td>POSLINE:</td><td>{MA.POSLINE}</td></tr>
<table>
'''

  texto += f"<hr>" # divider line

  # split table

  texto += "<table style='width:100%; padding:4px; padding-top:8px;'>\n"
  texto += "<tr>\n"

  # left
  texto += "<td style='width:35%; vertical-align:top;'>\n"
  keys = MA.FIRMWARECAPS.keys()
  for key in keys:
    value = MA.FIRMWARECAPS[key]
    texto += f"{key}: {value}<br>\n"
  texto += f"<br>\n"
  texto += "</td>\n"

  # right
  texto += "<td style='width:50%; vertical-align:top;'>\n"
  texto += f"Status log (descending)<br>\n"
  texto += f"<br>\n"
  for line in statustail:
    texto += f"{line}<br>\n"
  if statustail:
    texto += f"...<br>\n"
  texto += "</td>\n"

  texto += "</tr>\n"
  texto += "</table>\n"

#========================================================
