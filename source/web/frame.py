#
# frame.py * GPLv3 (C) 2021 librehof.com
#

from web import *

#========================================================

def header(reqtype, resptype, req, resp):

  pass

#========================================================

def top(req, formi, texto):

  if req.url == "/":
    key = "status"
  else:
    key = urlleaf(req.url)

  onload=""
  pollms = APP.WEBPOLLMS
  if pollms:
    if pollms < 50:
      pollms = 50
    onload = f"tick({pollms},'{key}');"

  statusclass = "frametab0"
  tableclass = "frametab0"
  infoclass = "frametab0"

  if req.url.endswith("table"):
    tableclass = "frametab1"
  elif req.url.endswith("info"):
    infoclass = "frametab1"
  else:
    statusclass = "frametab1"

  #-------------------------------
  texto += f'''\
<html>

<head>
<title>{APP.TITLE} {APP.VERSION}</title>
<link rel="icon" type="image/png" sizes="64x64" href="/favicon"/>
<link rel="icon" type='image/png' sizes="256x256" href="/icon"/>
<link rel="stylesheet" href="style.css"/>
<script type="text/javascript" src="script.js" defer></script>
</head>

<body onload="{onload}">
<table class="frametable">
<tr>
 <td class="frametitle">  
  {APP.TITLE}<br><span style="font-size:16px;"><a href="http://librehof.com">librehof.com</a></span>
 </td>
 <td class="frametabs">
  <a id="statustab" class="{statusclass}" href="/">Status</a>
  <a id="tabletab" class="{tableclass}" href="/table">Table</a> 
  <a id="infotab" class="{infoclass}" href="/info">Info</a>
 </td>
 <td class="framestatus">
  <b><tt><span id="status" class="statusbox">---</span></tt></b>
  <span id="hms" class="hmsbox">hh:mm:ss</span>
 </td> 
</tr>
</table>
<br>
''' #-----------------------------

def bottom(req, formi, texto):

  #-------------------------------
  texto += f'''\
<div id="graycover" class="graycover"></div>
</body>

</html>
''' #-----------------------------

#========================================================

TABMAXPX = 600
TABMAXMM = 600
TABWIPX = 600
TABHIPX = 600

def px(mm):

  global TABMAXPX
  global TABMAXMM
  k = TABMAXPX/TABMAXMM
  return int(mm*k)

def addgrid(svghtml, grid, pos, offset, mpos):

  global TABMAXPX
  global TABMAXMM
  global TABWIPX
  global TABHIPX

  x = 0
  if offset[0]:
    x = offset[0]
  y = 0
  if offset[1]:
    y = offset[1]
  z = 0
  if offset[2]:
    z = offset[2]

  if WS.GRID == grid:
    gridcolor = "#ffffff" # "#ffff40"
    linecolor = "#fffff0"
    gstyle = "bold"
  else:
    gridcolor = "#ffffff" # "#e8e8e8"
    linecolor = "#707070"
    gstyle = "normal"

  svghtml += f"<text x='{TABWIPX+24}' y='{px(MA.DIMY)-50-pos*105}' fill='{gridcolor}' font-weight='{gstyle}'>G{grid}</text>"
  if x != 0 or y != 0 or grid == 54 or WS.GRID == grid:
    svghtml += f"<line x1='{TABWIPX+16}' y1='{px(MA.DIMY)-55-pos*105}' x2='{10+px(x)}' y2='{2+px(MA.DIMY-y)}' style='stroke:{linecolor};stroke-width:2;stroke-dasharray:4;'/>"
    svghtml += f"<circle cx='{10+px(x)}' cy='{2+px(MA.DIMY-y)}' r='4' stroke='white' stroke-width='2' fill='none'/>"
    if WS.GRID == grid:
      svghtml += f"<line x1='{10+px(x)}' y1='{2+px(MA.DIMY-y)}' x2='{10+px(x)+30}' y2='{2+px(MA.DIMY-y)}' style='stroke:#c0c0c0;stroke-width:1;'/>"
      svghtml += f"<line x1='{10+px(x)}' y1='{2+px(MA.DIMY-y)}' x2='{10+px(x)}' y2='{2+px(MA.DIMY-y)-30}' style='stroke:#c0c0c0;stroke-width:1;'/>"
      svghtml += f"<line x1='{10+px(x)}' y1='{2+px(MA.DIMY-y)}' x2='{10+px(mpos[0])}' y2='{2+px(MA.DIMY-mpos[1])}' style='stroke:#ffff20;stroke-width:1;'/>"
  svghtml += f"<text x='{TABWIPX+24}' y='{px(MA.DIMY)-32-pos*105}' fill='#ffd080'>x{x:07.2f}</text>"
  svghtml += f"<text x='{TABWIPX+24}' y='{px(MA.DIMY)-16-pos*105}' fill='#ffd080'>y{y:07.2f}</text>"
  svghtml += f"<text x='{TABWIPX+24}' y='{px(MA.DIMY)-pos*105}' fill='#ffd080'>z{z:07.2f}</text>"

def makesvg():

  global TABMAXPX
  global TABMAXMM
  global TABWIPX
  global TABHIPX

  svghtml = Text()

  #if MA.DIMX > MA.DIMY:
  #  TABMAXMM = MA.DIMX
  #else:
  #  TABMAXMM = MA.DIMY
  TABMAXMM = MA.DIMY

  TABWIPX = px(MA.DIMX)
  TABHIPX = px(MA.DIMY)

  mpos = WS.MPOS()

  svghtml += f"<svg width='{TABWIPX+110+4}' height='{TABHIPX+8}'>"
  svghtml += f"<rect x='10' y='2' width='{TABWIPX}' height='{TABHIPX}' style='stroke-width:3;stroke:gray;'/>"
  addgrid(svghtml, 54, 0, [0,0,0], mpos)
  addgrid(svghtml, 55, 1, WS.OFFSET55, mpos)
  addgrid(svghtml, 56, 2, WS.OFFSET56, mpos)
  addgrid(svghtml, 57, 3, WS.OFFSET57, mpos)
  addgrid(svghtml, 58, 4, WS.OFFSET58, mpos)
  addgrid(svghtml, 59, 5, WS.OFFSET59, mpos)
  svghtml += f"<circle cx='{10+px(mpos[0])}' cy='{2+px(MA.DIMY-mpos[1])}' r='5' stroke='red' stroke-width='2' fill='none'/>"
  svghtml += f"<circle cx='{10+px(mpos[0])}' cy='{2+px(MA.DIMY-mpos[1])}' r='2' stroke-width='0' fill='#ffff20'/>"
  svghtml += f"</svg>"

  return svghtml.pop()

#========================================================

def poll(req, mapi, jso):

  key = ""
  if "key" in mapi:
    key = mapi["key"]

  if APP.WEBRELOADED:
    # dev mode reload
    APP.WEBRELOADED = False # ack
    jso += "reloadpage();\n"
    return

  nowdt = hostnowdt()
  hms = nowdt.strftime("%H:%M:%S")
  day = nowdt.strftime("%d")
  day = day.lstrip("0")
  if day.endswith("1") and day != "11":
    day += ":st"
  elif day.endswith("2") and day != "12":
    day += ":nd"
  elif day.endswith("3") and day != "13":
    day += ":rd"
  else:
    day += ":th"

  xpos = f"{WS.WPOS[0]:.2f}"
  ypos = f"{WS.WPOS[1]:.2f}"
  zpos = f"{WS.WPOS[2]:.2f}"

  apos = f"{WS.EPOS[0]:.2f}"
  bpos = f"{WS.EPOS[1]:.2f}"
  cpos = f"{WS.EPOS[2]:.2f}"

  # extra state

  disabled = ""
  if MA.DISABLED:
    disabled = "DISABLED"

  unsafe = ""
  if MA.UNSAFE:
    unsafe = "UNSAFE!"

  if MA.MESSAGE:
    msgtime = MA.MSGTIME
    message = MA.MESSAGE
  else:
    msgtime = MA.STATUSTIME
    message = MA.STATUS

  jso += f'''\
settext("day","{day}");
settext("hms","{hms}");
settext("status","{MA.STATUS}");
settext("xpos","{xpos}");
settext("ypos","{ypos}");
settext("zpos","{zpos}");
settext("apos","{apos}");
settext("bpos","{bpos}");
settext("cpos","{cpos}");
settext("msgtime","{msgtime}");
settext("message","{message}");
settext("grid","G{WS.GRID}");
settext("disabled","{disabled}");
settext("unsafe","{unsafe}");
settext("started","{MA.STARTED}");
settext("ended","{MA.ENDED}");
'''

  if key == "table":
    svghtml = makesvg()
    jso += f'''sethtml("svghtml","{svghtml}");\n'''

  return

#========================================================
