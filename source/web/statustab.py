from web import *

#========================================================

def body(req, formi, texto):

  apos = ""
  if MA.AAXIS:
    #-----------------------------
    apos = f'''\
<tr>
  <td class="poskey"><span class="keybox">A</span></td>
  <td class="posvalue"><tt><span id="apos" class="valuebox">-</span></tt></td>
</tr>
''' #-----------------------------


  bpos = ""
  if MA.BAXIS:
    #-----------------------------
    bpos = f'''\
<tr>
  <td class="poskey"><span class="keybox">B</span></td>
  <td class="posvalue"><tt><span id="bpos" class="valuebox">-</span></tt></td>
</tr>
''' #-----------------------------


  cpos = ""
  if MA.BAXIS:
    #-----------------------------
    cpos = f'''\
<tr>
  <td class="poskey"><span class="keybox">C</span></td>
  <td class="posvalue"><tt><span id="cpos" class="valuebox">-</span></tt></td>
</tr>
''' #-----------------------------


  #-------------------------------
  texto += f'''\
<table style="width:99%; margin-left:2px;"><tr>
<td style="width:10%; white-space:nowrap; vertical-align:top;" class="statustd">
 <table style="width:100%;">
 <tr>
  <td class="poskey"><span class="keybox">X</span></td>
  <td class="posvalue"><tt><span id="xpos" class="valuebox">-</span></tt></td>
 </tr>
 <tr>
  <td class="poskey"><span class="keybox">Y</span></td>
  <td class="posvalue"><tt><span id="ypos" class="valuebox">-</span></tt></td>
 </tr>
 <tr>
  <td class="poskey"><span class="keybox">Z</span></td>
  <td class="posvalue"><tt><span id="zpos" class="valuebox">-</span></tt></td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
 </tr> 
 {apos}
 {bpos}
 {cpos}
 </table>
</td>
<td>&nbsp;&nbsp;&nbsp;</td>
<td style="vertical-align:top; text-align:left; white-space:nowrap;" class="statustd">
 <span style="margin-top:8px;" id="day" class="daybox">day:th</span><br>
 <span id="started" class="statebox">-</span><br>
 <span id="ended" class="statebox">-</span><br>
 <br>
 <span id="grid" class="statebox">-</span><br>
 <br>
 <span id="unsafe" class="statebox">-</span><br>
 <span id="disabled" class="statebox">-</span><br>
</td>
</tr>
</table>
<div class="statusline">&nbsp;<span id="msgtime">-</span> &#x2022; <span id="message">-</span></div>
''' #-----------------------------

#========================================================
