// marlindroid1/script.js
// GPLv3 (C) 2021 librehof.com

//---------------------------------

function graycover(on)
{
  if(on) {
    document.getElementById('graycover').style.display='block';
  }
  else {
    document.getElementById('graycover').style.display='none';
  }
}

//---------------------------------

function applyform(form)
{
  graycover(true);
  document.getElementById(form).submit();
}

//---------------------------------

function loadpage(url)
{
  graycover(true);
  window.location.href = url;
}

//---------------------------------

function reloadpage()
{
  graycover(true);
  window.location.reload(true); // true = skip cache
}

//---------------------------------

function settext(id, content)
{
  var tag = document.getElementById(id);
  if (tag) tag.textContent = content;
}

//---------------------------------

function sethtml(id, innerhtml)
{
  var tag = document.getElementById(id);
  if (tag) tag.innerHTML = innerhtml;
}

//---------------------------------

function poll(args, timeout)
{
  const req = new XMLHttpRequest();
  req.onload = function() {
    if(req.status >= 200 && req.status < 300) {
      eval(req.responseText); // execute javascript response
    }
  };
  req.open('POST', '/poll');
  req.timeout = timeout;
  req.setRequestHeader('Content-Type', 'application/json');
  req.send(JSON.stringify(args));
}

//---------------------------------

var ongoingtick = false;

function tick(periodms, key)
{
  const args = { "key": key };
  poll(args, periodms);
  var timer = setInterval(
    function() {
      if ( !ongoingtick ) {
        ongoingtick = true;
        try {
          poll(args, periodms*5);
        }
        catch (error) {
          console.error(error);
        }
        ongoingtick = false;
      }
    },
    periodms
  );
}

//---------------------------------
