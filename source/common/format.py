# python3.6+
#
# format * Apache 2.0 (C) 2021 librehof.com
#
# jval
# mval, mcontent
# TreeText
# JsonText
#

from basic import *
from buffer import *

#==========================================================

# any object => json scalar value (str), possibly quoted

def jval(item, decimals=None):

  if item is None:
    return "null"

  elif isinstance(item, bool):
    if item:
      return "true"
    else:
      return "false"

  elif isinstance(item, int):
    if abs(item) >= 2**53:
      raise Exception(f"Json integer too large: {abs(item)}")
    return str(item)

  elif isinstance(item, float):
    if decimals:
      return dstr(item, decimals)
    else:
      return str(item)

  # json escape and quote
  text = textwash(item) # remove ctrl chars
  text = text.replace('\\', '\\\\') # backslash
  #value = value.replace('\b', '\\b')  # backspace
  #value = value.replace('\f', '\\f')  # form feed
  text = text.replace('\n', '\\n')  # newline
  #value = value.replace('\r', '\\r')  # carriage return
  text = text.replace('\t', '\\t')  # tab
  text = text.replace('"',  '\\"')  # double quote
  return f'"{text}"'

#==========================================================

# any object => html/xml attribute value (str), quoted

def mval(item, decimals=None):

  if item is None:
    return '""'

  elif isinstance(item, bool):
    if item:
      return '"true"'
    else:
      return '"false"'

  elif isinstance(item, int):
    return f'"{item}"'

  elif isinstance(item, float):
    if decimals:
      number = dstr(float, decimals)
    else:
      number = str(item)
    return '"{number}"' # float

  # ml escape
  text = stringwash(item) # remove ctrl chars
  text = text.replace("&", "&amp;")
  text = text.replace("<", "&lt;")
  text = text.replace(">", "&gt;")

  # ml quote
  if '"' in text:
    if "'" in text:
      text = text.replace("'", "&apos;")
    return f"'{text}'" # single quote
  else:
    return f'"{text}"' # double quote

#----------------------------------------------------------

# any object => html/xml content, not quoted

def mcontent(item):

  if item is None:
    return ""

  elif isinstance(item, bool):
    if item:
      return "true"
    else:
      return "false"

  elif isinstance(item, (int,float)):
    return str(item)

  # ml escape
  text = textwash(item)
  text = text.replace("&", "&amp;")
  text = text.replace("<", "&lt;")
  text = text.replace(">", "&gt;")
  return text

#==========================================================

class TreeText:

  def __init__(self, output=None):

    if not output:
      output = Text()
    self.output = output
    self.counters = [0] # list with counters
    self.indent = ""
    self.isrow = False

  #--------------------------------------------------------

  # current level (root = 0)

  def level(self):

    return len(self.counters) - 1

  #--------------------------------------------------------

  # row count at current level

  def count(self):

    level = self.level()
    return self.counters[level]

  #--------------------------------------------------------

  # helper

  def down(self):

    self.counters.append(0)
    self.indent += "  "

  #--------------------------------------------------------

  # helper

  def up(self):

    level = self.level() - 1
    if level < 0:
      raise Exception("Can not move up from root")
    self.indent = self.indent[:-2]
    self.counters = self.counters[:-1]
    self.counters[level] += 1

  #--------------------------------------------------------

  # helper

  def line(self, string):

    if self.isrow:
      raise Exception("Line inside row")
    self.output += f"{self.indent}{string}\n"

  #--------------------------------------------------------

  # increments counter

  def row(self, string):

    self.line(string)
    level = self.level()
    self.counters[level] += 1

  #--------------------------------------------------------

  # increments counter

  def startrow(self):

    self.output += self.indent
    level = self.level()
    self.counters[level] += 1
    self.isrow = True

  #--------------------------------------------------------

  def endrow(self):

    self.output += "\n"
    self.isrow = False

  #--------------------------------------------------------

  # increments counter

  def start(self, string):

    self.row(string)
    self.down()

  #--------------------------------------------------------

  def end(self, string):

    self.up()
    self.line(string)

  #--------------------------------------------------------

  def __add__(self, part):

    self.output += part
    return self

  #--------------------------------------------------------

  def __repr__(self):

    return repr(self.output)

  #--------------------------------------------------------

  def __str__(self):

    return str(self.output)

#==========================================================

class JsonText(TreeText):

  def __init__(self, output=None):

    super().__init__(output)

  #--------------------------------------------------------

  # increments counter

  def row(self, string):

    level = self.level()
    count = self.counters[level]
    if count:
      string = f",{string}"
    self.line(string)
    self.counters[level] += 1

  #--------------------------------------------------------

  # increments counter

  def startrow(self):

    level = self.level()
    count = self.counters[level]
    if count:
      self.output += self.indent
    else:
      self.output += f"{self.indent},"
    self.counters[level] += 1
    self.isrow = True

  #--------------------------------------------------------

  def keystart(self, key, string):

    string = f"{jval(key)}: {string}"
    self.row(string)
    self.down()

  #--------------------------------------------------------

  # value (list)

  def val(self, value, decimals=None):

    string = jval(value, decimals)
    self.row(string)

  #--------------------------------------------------------

  # key value (map)

  def keyval(self, key, value, decimals=None):

    string = f"{jval(key)}: {jval(value,decimals)}"
    self.row(string)

#==========================================================

class MarkupText(TreeText):

  def __init__(self, output=None):

    super().__init__(output)
    self.iscontent = False

  #--------------------------------------------------------

  def startcontent(self, string):

    self.startrow()
    self.output += string
    self.iscontent = True

  #--------------------------------------------------------

  def endcontent(self, string):

    if not self.iscontent:
      raise Exception("End outside content")
    self.iscontent = False
    self.output += string
    self.endrow()

  #--------------------------------------------------------

  def end(self, string):

    if self.iscontent:
      raise Exception("End inside content")
    self.up()
    self.line(string)

  #--------------------------------------------------------

  def __add__(self, part):

    if self.iscontent:
      self.output += mcontent(part)
    else:
      self.output += part
    return self

#==========================================================
