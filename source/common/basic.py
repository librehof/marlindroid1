#python3.6+
#
# basic * Apache 2.0 (C) 2021 librehof.com
#
# - time
# - data
# - file
# - exceptions
#
# when using zonedt() with tzname (instead of offset):
# - python-dateutil must be installed
# - [sudo] pip[3] install python-dateutil [--upgrade] [--user]
# - available tz names may be system dependent
# - underlying zone database must be kept fresh (!)
# - see /usr/share/zoneinfo on debian/ubuntu for example
# - see IANA/Olson database for standard tz names
#

# essential standard libraries
import os
import sys
import stat
import codecs
import locale
import errno
import shutil
import time
import datetime
from datetime import date as Date # gregorian date
from datetime import datetime as Datetime # gregorian date+time
from datetime import timedelta as Timedelta # days+seconds+ms
import queue
import weakref
import math
from math import isclose
import random
import urllib
import urllib.parse
import hashlib
import uuid
import json
import subprocess
import glob
import struct

# put OS charset in global variable
OSCHARSET = locale.getpreferredencoding(False)

#==========================================================

# current seconds (float) since arbitrary boot time, never 0

def mononow():

  mono = time.monotonic()
  if not mono:
    mono = 0.000000001
  return  mono

#----------------------------------------------------------

# current duration in seconds (float) from a given mono time

# use for continues interval measurements

def monofrom(monostart):

  monoend = time.monotonic()
  duration = monoend - monostart
  return  duration

#==========================================================

# current utc time (float seconds) since unix epoch 1970-01-01

# do not use for continues interval measurements (see mononow)
# may jump on time adjustments and leap seconds!

def utcnow():

  return time.time()

#----------------------------------------------------------

# current utc time (float seconds) + host offset (int seconds)

# returns (utc, offset)

def utcoffsetnow():

  utc = time.time()
  dhost = Datetime.now() # measured with some delay (not perfect)
  dutc = Datetime.utcfromtimestamp(utc)
  delta = dhost - dutc
  offset = delta.total_seconds()
  offset = int(round(offset,-1)) # rounded to 10-seconds
  return (utc, offset)

#----------------------------------------------------------

# current utc time as gregorian Datetime (dt)

# use for direct string conversion with Datetime.strftime(format)

# no zone info included, not universally comparable!
# see zonedt() to include zone info

def utcnowdt():

  return Datetime.utcnow()

# compact utc day string (yyyymmddU)

def utcdaystr():

  dt = utcnowdt()
  return dt.strftime("%Y%m%dU")

# compact utc time string (yyyymmddUhhmmss)

def utctimestr():

  dt = utcnowdt()
  return dt.strftime("%Y%m%dU%H%M%S")

#----------------------------------------------------------

# host's current local time as gregorian Datetime (dt)

# use for direct string conversion with Datetime.strftime(format)

# no zone info included, not universally comparable!
# see zonedt() and utcoffsetnow() to include zone info

def hostnowdt():

  return Datetime.now()

# compact host day string (yyyymmdd)

def hostdaystr():

  dt = hostnowdt()
  return dt.strftime("%Y%m%d")

# compact utc time string (yyyymmddThhmmss)

def hosttimestr():

  dt = hostnowdt()
  return dt.strftime("%Y%m%dT%H%M%S")

#----------------------------------------------------------

# utc + offset|tzname to gregorian Datetime (dt)

# returns Datetime with zone info (zone aware)
# Datetime.utcoffset() will return correct offset from utc
# Datetime.strftime('%Z') will return "short" zone name
# examples: UTC, CEST, UTC+02:00

# see utcoffsetnow() to get host's current offset
# python-dateutil must be installed when using tz names!

def zonedt(utc, offsetorname=None):

  if offsetorname is None:
    # zone info = utc
    zdt = Datetime.fromtimestamp(utc, datetime.timezone.utc)
  elif isinstance(offsetorname,int):
    # zone info = offset from utc
    zone = datetime.timezone(Timedelta(seconds=offsetorname))
    zdt = Datetime.fromtimestamp(utc, zone)
  else:
    # zone info = tz name
    import dateutil.tz
    name = offsetorname
    zone = dateutil.tz.gettz(name)
    zdt = Datetime.fromtimestamp(utc, zone)
  return zdt

#----------------------------------------------------------

# Date/Datetime => isoweek

# the first week (1) of a year is the first calendar week
# containing a thursday (52 or 53 full weeks)

def datetoisoweek(date):

  iso = date.isocalendar() # (year,week,weekday)
  return iso[1]

#----------------------------------------------------------

# Date/Datetime => isoweekday (1=monday .. 7=sunday)

def datetoisoweekday(date):

  iso = date.isocalendar() # (year,week,weekday)
  return iso[2]

#----------------------------------------------------------

# abbreviated month of year in english (used by http)

ENGLISHSHORTMONTHS = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

def englishshortmonth(month):

  if 1 <= month <= 12:
    return ENGLISHSHORTMONTHS[month-1]
  else:
    raise Exception(f"Illegal month-of-year number ({month})")

#----------------------------------------------------------

# abbreviated day of week in english (used by http)

ENGLISHSHORTWEEKDAYS = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")

def englishshortweekday(isoweekday):

  if 1 <= isoweekday <= 7:
    return ENGLISHSHORTWEEKDAYS[isoweekday-1]
  else:
    raise Exception(f"Illegal ISO weekday number ({isoweekday})")

#==========================================================

# float comparison by n digits (relative precision)

def eq9(number1, number2):

  delta = abs(number1-number2)
  limit = max(abs(number1),abs(number2)) * 1e-9
  return delta <= limit

def eq12(number1, number2):

  delta = abs(number1-number2)
  limit = max(abs(number1),abs(number2)) * 1e-12
  return delta <= limit

def eq15(number1, number2):

  delta = abs(number1-number2)
  limit = max(abs(number1),abs(number2)) * 1e-15
  return delta <= limit

#----------------------------------------------------------

# float+decimals comparison (absolute precision)

# half-even rounding
# like dstr(n1,d1) == dstr(n2,d2)

def eqd(number1, number2, decimals):

  rounded1 = round(number1,decimals)
  rounded2 = round(number2,decimals)
  return eq15(rounded1,rounded2)

#----------------------------------------------------------

# float+decimals to string

# half-even rounding
# selectable decimal point, dot is default
# +/- number if delta is True

def dstr(number, decimals, dp=".", delta=False):

  if number is None:
    return None

  if isinstance(number,int):
    number = float(number)

  if not isinstance(number,float):
    typename = type(number).__name__
    raise Exception(f"Expected float, got {typename}")

  # float to string
  if number == 0.0:
    number = 0.0  # eliminate -0.0
  number = round(number, decimals) # half-even
  if delta:
    form = f"%+.{decimals}f"
  else:
    form = f"%.{decimals}f"
  string = form % number

  # decimal point
  if dp != ".":
    string = string.replace(".", dp)

  return string

#==========================================================

# returns bom signature, or b'' for bare utf8

def checkbom(raw):

  if raw.startswith(codecs.BOM_UTF16_LE):
    return codecs.BOM_UTF16_LE
  elif raw.startswith(codecs.BOM_UTF16_BE):
    return codecs.BOM_UTF16_BE
  elif raw.startswith(codecs.BOM_UTF8):
    return codecs.BOM_UTF8
  else:
    return b''

#-----------------------------------------------------------

# raw utf8/utf16le/utf16be to text (using bom logic)

# will ensure pure newline \n

def utftotext(raw):

  bom = checkbom(raw)
  bomlen = len(bom)
  if bom == codecs.BOM_UTF16_LE:
    raw = raw[bomlen:]
    text = raw.decode("utf-16le")
  elif bom == codecs.BOM_UTF16_BE:
    raw = raw[bomlen:]
    text = raw.decode("utf-16be")
  elif bom == codecs.BOM_UTF8:
    raw = raw[bomlen:]
    text = raw.decode("utf-8")
  else:
    text = raw.decode("utf-8")
  text = text.replace("\r", "")
  return text

#-----------------------------------------------------------

# "raw" byte string to "text" unicode string

# assumes utf/bom encoding if no charset
# will ensure pure newline \n

def rawtotext(raw, charset=None):

  if charset and not charset in ("utf","utf-16"):
    text = raw.decode(charset)
  else:
    text = utftotext(raw)
  text = text.replace("\r", "")
  return text

#==========================================================

# str(item) to raw byte string, with selected charset and newline

# assumes utf-8 if no charset
# will use utf-16le with bom if "utf-16"
# default newline:
#  utf-16 => \r\n
#  others => \n
# use os.linesep for os newline

def makeraw(item, charset=None, newline=None):

  if not charset or charset == "utf":
    charset = "utf-8"

  if not newline:
    if charset.startswith("utf-16"):
      newline = "\r\n"
    else:
      newline = "\n"

  text = str(item)
  text = text.replace("\r", "")
  if newline != "\n":
    text = text.replace("\n", newline)

  if charset == "utf-16":
    raw = text.encode("utf-16le")
    return codecs.BOM_UTF16_LE + raw
  else:
    return text.encode(charset)

#-----------------------------------------------------------

# str(item) to utf8 byte string (without bom)

# default newline: \n

def makeutf8(item, newline=None):

  return makeraw(item, "utf-8", newline)

#-----------------------------------------------------------

# str(item) to utf16le byte string with bom

# default newline: \r\n

def makeutf16(item, newline=None):

  return makeraw(item, "utf-16", newline)

#==========================================================

# verifies A-Z, a-z, 0-9, _ and optionally - and .
# double dot/dash is never allowed
# text="." or text="-" is never allowed
# returns True or False

def textislabel(text, allowdot=False, allowdash=False, allownumberstart=False):

  if not text:
    return False

  if text=="." or text=="-":
    return False

  if not allownumberstart:
    # must start without number digit/dash
    code = text[0]
    if "0" <= code <= "9" or code == "-":
      return False

  for code in text:
    if "A" <= code <= "Z" or "a" <= code <= "z" or code == "_":
      continue
    elif "0" <= code <= "9":
      continue
    elif allowdot and code == ".":
      continue
    elif allowdash and code == "-":
      continue
    else:
      return False

  # check double dot/dash
  if allowdash and "--" in text:
    return False
  if allowdot and ".." in text:
    return False

  return True

#-----------------------------------------------------------

# returns int or preset (None)

def textasint(text, preset=None):

  try:
    return int(text)
  except:
    return preset

#-----------------------------------------------------------

# returns float or preset (None)

def textasfloat(text, preset=None):

  try:
    return float(text)
  except:
    return preset

#==========================================================

# replace control chars other then tabs (\t) and newlines (\n)

def textwash(item, ctrlfiller="?"):

  if not item:
    return ""

  text = str(item)
  text = text.replace("\r", "")

  replace = False
  for code in text:
    if ord(code) < 32 and code != "\t" and code != "\n":
      replace = True
      break

  if not replace:
    return text # done

  # full scan

  if ctrlfiller is None:
    ctrlfiller = ""

  codes = []
  for code in text:
    if ord(code) < 32 and code != "\t" and code != "\n":
      codes.append(ctrlfiller)
    else:
      codes.append(code)

  text = "".join(codes)
  return text

#-----------------------------------------------------------

# replace non-ascii chars (code points > 7-bit)

def asciiwash(item, filler="?"):

  if not item:
    return ""

  text = str(item)

  replace = False
  for code in text:
    if ord(code) > 127:
      replace = True
      break

  if not replace:
    return text # done

  # full scan

  if filler is None:
    filler = ""

  codes = []
  for code in text:
    if ord(code) > 127:
      codes.append(filler)
    else:
      codes.append(code)

  text = "".join(codes)
  return text

#-----------------------------------------------------------

# replace control chars (will keep tabs by default)

def linewash(item, ctrlfiller="?", newlinefiller=" ", tabfiller=None):

  if not item:
    return ""

  if newlinefiller is None:
    newlinefiller = ""

  text = str(item)
  text = text.replace("\r", "")
  text = text.replace("\n", newlinefiller)
  if tabfiller:
    text = text.replace("\t", tabfiller)

  replace = False
  for code in text:
    if ord(code) < 32 and code != "\t":
      replace = True
      break

  if not replace:
    return text # done

  # full scan

  if ctrlfiller is None:
    ctrlfiller = ""

  codes = []
  for code in text:
    if ord(code) < 32 and code != "\t":
      codes.append(ctrlfiller)
    else:
      codes.append(code)

  text = "".join(codes)
  return text

#-----------------------------------------------------------

# replace all control chars
# and optionally replace " ' and space

def stringwash(item, ctrlfiller="?", doublefiller=None, singlefiller=None, spacefiller=None):

  if not item:
    return ""

  string = linewash(item, ctrlfiller, " ", " ")

  if doublefiller:
    string.replace('"', doublefiller)
  if singlefiller:
    string.replace("'", singlefiller)
  if spacefiller:
    string.replace(" ", spacefiller)

  return string

#-----------------------------------------------------------

# stringwash and replace * .. < > | ? : and space

def pathwash(item, spacefiller="-", pathfiller=""):

  if not item:
    return ""

  path = stringwash(item, "", "", "", spacefiller)

  path = path.replace('*', pathfiller)
  path = path.replace('<', pathfiller)
  path = path.replace('>', pathfiller)
  path = path.replace('|', pathfiller)
  path = path.replace('?', pathfiller)
  path = path.replace(':', pathfiller)

  while ".." in path:
    path = path.replace("..", ".")

  path = path.rstrip(".")

  if not path:
    path = "-"

  return path

#-----------------------------------------------------------

# a-z, A-Z, 0-9, . - _

def labelwash(item, filler=""):

  if not item:
    return ""

  text = str(item)

  if filler is None:
    filler = ""

  codes = []
  for code in text:
    if "A" <= code <= "Z" or "a" <= code <= "z":
      codes.append(code)
    elif "0" <= code <= "9":
      codes.append(code)
    elif code == "_" or code == "." or code == "-":
      codes.append(code)
    else:
      codes.append(filler)

  text = "".join(codes)
  return text

#-----------------------------------------------------------

# make limited single-line string from any object

# - convert to string if object
# - replace control chars
# - strip spaces at start and end
# - maxlen limit, keeping rightlen, adding cut mark

def shortify(item, maxlen=1000, rightlen=0, mark=".."):

  if not item:
    return ""

  text = str(item)
  line = linewash(text, "?", " ", ",")
  line = line.strip(" ")

  # limit length
  linelen = len(line)
  if linelen > maxlen:
    if mark is None:
      mark = ""
    leftlen = maxlen - rightlen - len(mark)
    left = line[:leftlen]
    if rightlen and rightlen > 0:
      # telescope cut
      right = line[-rightlen:]
      line = left + mark + right
    else:
      # end cut
      line = left + mark

  return line

#-----------------------------------------------------------

# traceback => source path or "nosource"

# returns compact source code path as <name>:line:line\:line...
# traceable if you know the specific version of your app
# only includes source code inside approot directory

def tbsource(tb, approot):

  # build source path

  source = ""
  try:
    # gather (name,pos) list from top-down stack
    stack = []
    while tb:
      frame = tb.tb_frame
      absname = pathabs(frame.f_code.co_filename)
      absdir = pathparent(absname)
      if absdir.startswith(approot):
        # part of managed source
        name = pathleaf(absname)
        if name.endswith(".py"):
          name = name[:-3] # remove extension
        pos = tb.tb_lineno
        pair = (name,pos)
        stack.insert(0, pair) # prepend
      tb = tb.tb_next # down one level

    # build bottom-up path from (name,pos) list
    last = ""
    for place in stack:
      name = place[0]
      pos = place[1]
      if not source:
        source = f"{name}:{pos}" # first (deepest)
      elif name == last:
        source += f":{pos}" # same file
      else:
        source += f"\\:{pos}" # new file
      last = name

  except:
    pass

  if not source:
    return "nosource"

  return source

#-----------------------------------------------------------

# exception => error message (with source path if tb and approot)

# try:
#   ...
# except Exception as e:
#   msg = estr(e, sys.exc_info()[2], approot)
#   print(msg)

def estr(e, tb=None, approot=None):

  msg = shortify(e, 200, 100)

  if tb and approot:
    source = tbsource(tb, approot)
    msg = f"{msg} {{{source}}}" # append source path

  return msg

#==========================================================

# hash functions, returns lower case hex string

def rawmd5(raw):

  return hashlib.md5(raw).hexdigest().lower()

def rawsha1(raw):

  return hashlib.sha1(raw).hexdigest().lower()

def rawsha256(raw):

  return hashlib.sha256(raw).hexdigest().lower()

#-----------------------------------------------------------

# text hash functions (utf8 with \n newline)

def textmd5(item):

  raw = makeutf8(item)
  return hashlib.md5(raw).hexdigest().lower()

def textsha1(item):

  raw = makeutf8(item)
  return hashlib.sha1(raw).hexdigest().lower()

def textsha256(item):

  raw = makeutf8(item)
  return hashlib.sha256(raw).hexdigest().lower()

#==========================================================

# close any open handle, with silent failure
# returns None

def silentclose(handle):

  if not handle:
    return None
  try:
    handle.close()
  finally:
    return None

#==========================================================

# url manipulation (url without query variables)

def urlparent(url):

  pos = url.rfind("/")
  if pos >= 0:
    return url[:pos]
  else:
    return ""

def urlleaf(url):

  pos = url.rfind("/")
  if pos >= 0:
    return url[pos+1:]
  else:
    return ""

def urljoin(base, addition):

  if base != "/":
    base = base.rstrip("/")
  addition = addition.strip("/")
  return f"{base}/{addition}"

#==========================================================

# file path manipulation

def pathjoin(base, addition):

  return os.path.join(base, addition)

def pathabs(path):

  return os.path.abspath(path)

def pathreal(path): # will resolve symlinks

  return os.path.realpath(path)

def pathparent(path):

  return os.path.dirname(path)

def pathleaf(path):

  return os.path.basename(path)

def pathlabel(path):

  leaf = pathleaf(path)
  return os.path.splitext(leaf)[0]

def pathext(path): # including dot

  return os.path.splitext(path)[1]

#==========================================================

def pathexists(path):

  return os.path.exists(path)

#----------------------------------------------------------

def pathisdir(path):

  return os.path.isdir(path)

#----------------------------------------------------------

# true for regular files, follows symlinks
# not true for resource files (devices, sockets, ...)

def pathisfile(path):

  return os.path.isfile(path)

#----------------------------------------------------------

# true for regular files, follows symlinks
# not true for resource files (devices, sockets, ...)

def pathislink(path):

  return os.path.islink(path)

#----------------------------------------------------------

# linux specific

def pathmode(path):

  try:
    info = os.stat(path)
    return info.st_mode
  except:
    return 0x1ff # 777

#----------------------------------------------------------

# linux specific

def pathisdevice(path):

  try:
    info = os.stat(path)
    return stat.S_ISBLK(info.st_mode)
  except:
    return False

#==========================================================

# parent must exist

def ensuredir(path):

  path = pathabs(path)
  if not pathexists(path):
    os.mkdir(path)
    return True
  else:
    return False

#----------------------------------------------------------

# directory must exist

def ensurefile(path):

  path = pathabs(path)
  parent = pathparent(path)
  ensuredir(parent)
  if not pathexists(path):
    open(path,"wb").close()
    return True
  else:
    return False

#----------------------------------------------------------

# directory must exist

def ensurelink(linkfile, reference):

  linkfile = pathabs(linkfile)
  if pathexists(linkfile):
    if not pathislink(linkfile):
      raise Exception(f"Expected link: {linkfile}")
    current = os.readlink(linkfile)
    if current == reference:
      return False
    os.unlink(linkfile)
  os.symlink(reference, linkfile)
  return True

#----------------------------------------------------------

# returns last modified in utc

def filemodified(path):

  path = pathabs(path)
  return os.path.getmtime(path)

#----------------------------------------------------------

# returns logical byte size

def filesize(path):

  path = pathabs(path)
  return os.path.getsize(path)

#----------------------------------------------------------

# returns used byte size (linux specific?)

def filespace(path):

  path = pathabs(path)
  try:
    return os.stat(path).st_blocks*512
  except:
    return filesize(path)

#==========================================================

def copyfile(source, target):

  source = pathabs(source)
  target = pathabs(target)
  shutil.copyfile(source, target)

#----------------------------------------------------------

# copy source to target
# TODO: control behaviour of symlinks

def copytree(source, target):

  source = pathabs(source)
  target = pathabs(target)
  shutil.copytree(source, target)

#----------------------------------------------------------

# copy content in source into target
# creates target if not present
# TODO: control behaviour of symlinks

def copycontent(source, target):

  source = pathabs(source)
  target = pathabs(target)
  ensuredir(target)
  for entry in os.listdir(source):
    subsource = pathjoin(source, entry)
    subtarget = pathjoin(target, entry)
    shutil.copytree(subsource, subtarget)

#----------------------------------------------------------

# move source to target
# TODO: control behaviour of symlinks

def movetree(source, target):

  source = pathabs(source)
  target = pathabs(target)
  shutil.move(source, target)

#----------------------------------------------------------

# move content from source into target
# creates target if not present
# TODO: control behaviour of symlinks

def movecontent(source, target):

  source = pathabs(source)
  target = pathabs(target)
  ensuredir(target)
  for entry in os.listdir(source):
    subsource = pathjoin(source, entry)
    subtarget = pathjoin(target, entry)
    shutil.move(subsource, subtarget)

#----------------------------------------------------------

# remove directory or file if exists
# TODO: control behaviour of symlinks

def removetree(path):

  path = pathabs(path)
  if not pathexists(path):
    return
  shutil.rmtree(path)

#----------------------------------------------------------

# remove content inside path
# TODO: control behaviour of symlinks

def removecontent(path):

  path = pathabs(path)
  for entry in os.listdir(path):
    subpath = pathjoin(path, entry)
    shutil.rmtree(subpath)

#==========================================================

# load raw file

def loadraw(path):

  path = pathabs(path)
  if not pathexists(path):
    raise Exception(f"File not found: {path}")
  if not pathisfile(path):
    raise Exception(f"Not a regular file: {path}")

  with open(path, "rb") as handle:
    raw = handle.read()

  return raw

#----------------------------------------------------------

# load text with charset

# see rawtotext

def loadtext(path, charset=None):

  path = pathabs(path)
  raw = loadraw(path)
  return rawtotext(raw, charset)

#----------------------------------------------------------

# load utf8 without bom

# returns decoded text with \n as newline

def loadutf8(path):

  path = pathabs(path)
  return loadtext(path, "utf-8")

#----------------------------------------------------------

# load utf8/utf16le/utf16be (using bom logic)

# returns decoded text with \n as newline

def loadutf(path):

  path = pathabs(path)
  raw = loadraw(path)
  return utftotext(raw)

#==========================================================

# save raw bytes to file

def saveraw(path, raw):

  path = pathabs(path)
  with open(path,"wb") as handle:
    handle.write(raw)
    handle.flush()

#----------------------------------------------------------

# save text with selected charset and newline

# see makeraw

def savetext(path, text, charset, newline=None):

  path = pathabs(path)
  raw = makeraw(text, charset, newline)
  saveraw(path, raw)

#----------------------------------------------------------

# save utf8 without bom and \n newline

# default in Linux and Windows 10 version 1903+

def saveutf8(path, text):

  path = pathabs(path)
  raw = makeutf8(text)
  saveraw(path, raw)

#----------------------------------------------------------

# save utf16le with bom and \r\n newline

def saveutf16(path, text):

  path = pathabs(path)
  raw = makeutf16(text)
  saveraw(path, raw)

#==========================================================
