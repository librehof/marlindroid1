#python3.6+
#
# gcode * Apache 2.0 (C) 2021 librehof.com
#
# G-code helper functions
#

import os
import sys
import shutil

#==========================================================

# normalize gcode request

def gcode_normalizereq(req):

  req = req.strip() # remove head/tail space
  req = req.replace("  ", " ") # remove double-space

  req = req.upper() # make uppercase

  # expand "compact gcode"
  digit = False
  longreq = ""
  for ch in req:
    if ch == " ":
      continue
    if ch.isdigit() or ch == ".":
      # digit
      digit = True
      longreq += ch
    else:
      # no digit
      if digit:
        longreq += " "
        digit = False
      longreq += ch
  req = longreq

  return req

#----------------------------------------------------------

# get (key,value) from "<key>[:]<value>" string

def gcode_keyvalue(keyvalue):

  keyend = keyvalue.find(":")
  if keyend <= 0:
    return None
  key = keyvalue[:keyend]
  value = keyvalue[keyend+1:]
  return (key.strip(),value.strip())

#----------------------------------------------------------

# get float value from "<letter>[:]<value>" string

def gcode_lettervalue(lettervalue):

  if len(lettervalue) <= 1:
    return None
  if lettervalue[1] == ':':
    if len(lettervalue) <= 2:
      return None
    value = float(lettervalue[2:])
  else:
    value = float(lettervalue[1:])
  if not value: # avoid -0.0
    value = 0.0
  return value

#==========================================================

# scalar compare (defaults to 3 decimals)

def scalar_cmp(n, m, precision=0.001):

  if n-precision <= m <= n+precision:
    return True
  else:
    return False

#----------------------------------------------------------

# (x,y,z) vector addition

def xyz_add(v, w):

  return (v[0]+w[0], v[1]+w[1], v[2]+w[2])

#----------------------------------------------------------

# (x,y,z) vector negation

def xyz_neg(v):

  return (-v[0], -v[1], -v[2])

#----------------------------------------------------------

# (x,y,z) vector subtraction

def xyz_sub(v, w):

  return (v[0]-w[0], v[1]-w[1], v[2]-w[2])

#----------------------------------------------------------

# (x,y,z) vector to string (3 decimals)

def xyz_str(v):

  return f"{v[0]:.3f},{v[1]:.3f},{v[2]:.3f}"

#----------------------------------------------------------

# (x,y,z) vector compare (defaults to 3 decimals)

def xyz_cmp(v, w, precision=0.001):

  if v[0]-precision <= w[0] <= v[0]+precision:
    if v[1]-precision <= w[1] <= v[1]+precision:
      if v[2]-precision <= w[2] <= v[2]+precision:
        return True

  return False

#==========================================================
