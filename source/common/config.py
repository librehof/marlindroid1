# python3.6+
#
# config * Apache 2.0 (C) 2021 librehof.com
#
# Load property file into python object (as variables)
#
# Format:
#   key="abc" (string)
#   key=5 (int)
#   key=7.2 (float)
#   key=(x,y,z) (tuple)
#   key=True
#   key=False
#   key=None
#
# space is not allowed around the equal sign (key=value separator)
# => will allow bash scripts to "source" config
#
# TODO: simple mathematics like key=2.5+3.7
#

from basic import *

#==========================================================

def loadconfig(obj, location):

  config = loadutf8(location)

  lines = config.split("\n")
  for line in lines:
    line = line.strip()

    # remove any comment
    end = line.find("#")
    if end >= 0:
      line = line[:end]
      line = line.strip()

    if not line: # empty line
      continue

    # decode key=value
    keyvalue = line.split("=")
    if len(keyvalue) != 2:
      raise Exception(f"Config error in {location}: {line}")

    # key
    key = keyvalue[0]
    if not key or '0' <= key[0] <= '9':
      raise Exception(f"Key error in {location}: {line}")
    if " " in key:
      raise Exception(f"Key space error in {location}: {line}")
    for ch in key:
      if 'a' <= ch <= 'z':
        continue
      elif 'A' <= ch <= 'Z':
        continue
      elif '0' <= ch <= '9':
        continue
      elif ch == '_':
        continue
      raise Exception(f"Illegal key in {location}: {line}")

    # value
    value = keyvalue[1]
    if not value or value.startswith(" "):
      raise Exception(f"Value space error in {location}: {line}")
    if value.startswith('"') and value.endswith('"'):
      # string
      if len(value) < 2:
        raise Exception(f"String error in {location}: {line}")
      value = value[1:-1]
      value.replace('\\"', '"')
    elif value.startswith("(") and value.endswith(")"):
      value = value[1:-1]
      fields = value.split(",")
      numbers = []
      for field in fields:
        try:
          number = float(field)
        except:
          raise Exception(f"Number error in {location}: {line}")
        numbers.append(number)
      value = tuple(numbers)
    elif "." in value: # float
      try:
        value = float(value)
      except:
        raise Exception(f"Number error in {location}: {line}")
    elif value == "None":
      value = None
    elif value == "True":
      value = True
    elif value == "False":
      value = False
    elif value.isdigit(): # integer
      try:
        value = int(value)
      except:
        raise Exception(f"Integer error in {location}: {line}")
    else:
      raise Exception(f"Value error in {location}: {line}")

    # check any existing value is within scope (type wise)
    # we do not want to overwrite some complex object (!)
    try:
      current = getattr(obj, key)
      if current is None:
        pass
      elif isinstance(current,(bool,int,float,str,tuple)):
        pass
      else:
        raise Exception(f"Illegal key in {location}: {line}")
    except:
      pass

    # add/set object variable dynamically
    setattr(obj, key, value)

#==========================================================
