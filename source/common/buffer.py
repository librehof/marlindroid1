#python3.6+
#
# buffer * Apache 2.0 (C) 2021 librehof.com
#
# Raw() buffer holding byte content (bytes)
# Text() buffer holding unicode content (str)
#
# first-in:  <part><part>...
# first-out: <block><block>...
# block = [mark]<item>[mark]
#
# build up content part by part (push/+=) and pop blocks (get)
# no clear function, create new buffer when starting over
# single thread design!
#
# push parts:
#  self.push(part)
#  self += part # overridable behaviour
#
# observe:
#  self.content() # returns content (joined parts) without pop
#  self.available # number of bytes, or characters = code points
#
# pop all:
#  self.pop()
#
# pop blocks (non-hierarchical decoding):
#  self.getbylength()
#  self.getmark()
#  self.getbymark()
#  self.getwithmark()
#

#==========================================================

# general buffer

# initial: "" str (text) or b'' bytes (raw)

class Buffer:

  def __init__(self, initial, maxsize=None):

    if maxsize is None:
      maxsize = 1024*1024*1024 # default

    # check
    if isinstance(initial,str):
      maxsize /= 2 # adjust some for utf8
    elif isinstance(initial,bytes):
      pass
    else:
      typename = type(initial).__name__
      raise Exception(f"Buffer expected initial str or bytes, got {typename}")

    # readable
    self.available = 0
    self.maxsize = maxsize

    # internal
    self._base = initial
    self._parts = []

  #--------------------------------------------------------

  # add arbitrary part to buffer

  def push(self, part):

    if not part:
      return # quick

    partlen = len(part)
    if isinstance(self._base, str):
      part = str(part)
    else:
      part = bytes(part)

    # check max size
    if self.available+partlen > self.maxsize:
      raise Exception(f"Buffer too large ({self.available+partlen})")

    # append part
    if not self.available:
      self._base = part
    else:
      self._parts.append(part)
    self.available += partlen

  #--------------------------------------------------------

  # overrideable push (+=)

  def __add__(self, part):

    self.push(part)
    return self

  #--------------------------------------------------------

  def blank(self):

    if isinstance(self._base, str):
      return "" # text
    else:
      return b'' # raw

  #--------------------------------------------------------

  # get copy

  def content(self):

    count = len(self._parts)
    if not count:
      return self._base # quick

    # internal join
    tail = self.blank().join(self._parts)
    self._parts = [] # free memory
    self._base += tail

    return self._base

  #--------------------------------------------------------

  # pop all

  def pop(self):

    content = self.content()
    self._base = self.blank()
    return content

  #--------------------------------------------------------

  # pop length = itemlen + marklen(=0)
  # returns item, or None if not available

  def getbylength(self, itemlen, marklen=0):

    poplen = itemlen + marklen
    if poplen <= 0:
      return self.blank()

    if poplen > self.available:
      return None # not yet available (quick)

    # pop
    content = self.content()
    item = content[:itemlen]
    self._base = content[poplen:]
    self.available -= poplen
    return item

  #--------------------------------------------------------

  # pop constant mark
  # returns mark, or None if other mark or not available

  def getmark(self, mark):

    content = self.content()
    if content.startswith(mark):
      marklen = len(mark)
      return self.getbylength(marklen) # available
    else:
      return None # other mark or not yet available

  #--------------------------------------------------------

  # pop item + constant mark
  # returns item, or None if mark was not found

  def getbymark(self, mark):

    if not mark:
      raise Exception("Get from buffer without mark")

    content = self.content()
    itemlen = content.find(mark)
    if itemlen < 0:
      return None # not yet available

    marklen = len(mark)
    return self.getbylength(itemlen, marklen)

  #--------------------------------------------------------

  # pop item + constant mark
  # returns item + mark, or None if mark was not found

  def getwithmark(self, mark):

    if not mark:
      raise Exception("Get from buffer without mark")

    content = self.content()
    itemlen = content.find(mark)
    if itemlen < 0:
      return None # not yet available

    marklen = len(mark)
    return self.getbylength(itemlen+marklen)

  #--------------------------------------------------------

  def __bytes__(self):

    if isinstance(self._base, str):
      raise Exception("Can not convert text buffer to bytes")

    content = self.content()
    return bytes(content)

  #--------------------------------------------------------

  def __str__(self):

    #tail = self.blank().join(self._parts)
    #content = self._base + tail
    content = self.content()
    return str(content)

  #--------------------------------------------------------

  def __repr__(self):

    return f"available({self.available})"

#==========================================================

class Raw(Buffer):

  def __init__(self, maxsize=None):

    super().__init__(b'', maxsize)

#----------------------------------------------------------

class Text(Buffer):

  def __init__(self, maxsize=None):

    super().__init__("", maxsize)

#==========================================================
