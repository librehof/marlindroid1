#
# Marlin request/response handler
#
# GPLv3 (C) 2021 librehof.com
#

from app import *

#==========================================================

# firmwareio = SerialRemote | TcpRemote

class MarlinCaller():

  def __init__(self, firmwareio):

     self.firmwareio = firmwareio

     self.refreshedpos = True # found and successfully decoded "M114 R"
     self.refreshcounter = 0

     # to detect Idle status
     self.lastwpos = WS.WPOS
     self.lastepos = WS.EPOS

     # flush old
     self.firmwareio += "\n"
     millisleep(200)
     self.firmwareio.receive()
     garbage = self.firmwareio.pop()
     if not garbage:
       millisleep(200)
       self.firmwareio.receive()
       self.firmwareio.pop()

     # get firmware info (and check that we have marlin firmware on the wire)
     mresplines = self.rawcall("M115")
     ismarlin = False
     hascap = False
     MA.MACHINEDATA.clear()
     MA.FIRMWARECAPS.clear()
     for line in mresplines:
       kv = gcode_keyvalue(line)
       if not kv:
         continue
       if kv[0] == "Cap":
         hascap = True
         kv = gcode_keyvalue(kv[1])
         if kv:
           MA.FIRMWARECAPS[kv[0]] = kv[1]
       elif kv[0] == "FIRMWARE_NAME":
         ismarlin = True
         if " " in kv[1]:
           firmwarelist = kv[1].split(" ")
         else:
           firmwarelist = [kv[1]]
         if len(firmwarelist) >= 1:
           MA.FIRMWARENAME = firmwarelist[0]
           firmwarelist = firmwarelist[1:]
         if len(firmwarelist) >= 1 and "." in firmwarelist[0]:
           MA.FIRMWAREVERSION = firmwarelist[0]
           firmwarelist = firmwarelist[1:]
         if len(firmwarelist) >= 1 and firmwarelist[0].startswith("("):
           MA.FIRMWARETIMESTAMP = firmwarelist[0][1:]
           firmwarelist = firmwarelist[1:]
           pop = 0
           for item in firmwarelist:
             pop += 1
             if not item.endswith(")"):
               MA.FIRMWARETIMESTAMP += " " + item
             else:
               MA.FIRMWARETIMESTAMP += " " + item[:-1]
               break
           while pop:
             firmwarelist = firmwarelist[1:]
             pop -= 1
         for item in firmwarelist:
           kv = gcode_keyvalue(item)
           if kv:
             MA.MACHINEDATA[kv[0]] = kv[1]
       else:
         MA.MACHINEDATA[kv[0]] = kv[1]

     if not ismarlin:
       raise Exception("Unknown machine info, M115 did not return MACHINE_TYPE (not Marlin?)")

     if not hascap:
       raise Exception("Unknown capabilities, M115 did not return any Cap (not Marlin?)")

     MA.CLEAR()

     app.note(f"FIRMWARE: {MA.FIRMWAREINFO()}")
     app.note(f"MACHINE: {MA.MACHINEINFO()}")

     if MA.LIMITSENSORS():
       # make sure limit sensors are enabled
       mresplines = self.rawcall("M120")
       if len(mresplines)==1 and mresplines[0] == "ok":
         app.note("Activated limit sensors (M120)")
       else:
         app.warning("Unknown M120 response")
         MA.SETSTATUS("LIMIT", "Unknown M120 response")

     # TODO: configure?
     mresplines = self.rawcall("G90")
     if len(mresplines)==1 and mresplines[0] == "ok":
       app.note("Activated absolute positioning (G90)")
     else:
       app.warning("Unknown G90 response")
       MA.SETSTATUS("LIMIT", "Unknown G90 response")

     self.refreshstate()
     self.refreshpos()
     if "Count X:0 Y:0 Z:0" in MA.POSLINE:
       MA.SETSTATUS("Check", "Steppers at zero (not homed)")
     else:
       MA.SETSTATUS("Idle")

     WS.VALID = True

  #--------------------------------------------------------

  def resetstate(self):

    app.note("Control: Soft Reset")

    # flush garbage
    millisleep(200)
    self.firmwareio.receive()
    self.firmwareio.pop()

    MA.CLEAR()
    MA.SETSTATUS("Idle", "Soft Reset")
    self.refreshstate()

    if not MA.LIMITSTOP and MA.LIMITSENSORS():
      # enable limit sensors
      self.rawcall("M120")

  #--------------------------------------------------------

  def refreshstate(self):

    # check limit sensors
    if not MA.UNSAFE:
      triggered = ""
      mresplines = self.rawcall("M119")
      for line in mresplines:
        if ": TRIGGERED" in line:
          axis = line.replace(": TRIGGERED", " ")
          triggered += axis
      if triggered:
        MA.SETSTATUS("LIMITSTOP", triggered)
      elif MA.STATUS == "LIMITSTOP":
        MA.SETSTATUS("LIMIT")

  #--------------------------------------------------------

  # refresh real-time position (WPOS and EPOS)

  def refreshpos(self, fullcheck=True):

    self.refreshedpos = False
    self.rawcall("M114 R")

    if not self.refreshedpos:
      MA.SETSTATUS("ERROR", "Failed to refresh position with M114 R")
      raise Exception("Failed to refresh position with M114 R")

    if not fullcheck:
      return

    # periodically check limit sensors (when not active)
    if MA.STATUS == "Idle" or MA.ERROR:
      self.refreshcounter += 1
      if self.refreshcounter >= 8:
        self.refreshcounter = 0
      if self.refreshcounter == 0:
        self.refreshstate()

    now = utcnow()

    if MA.HOLD:
      MA.LASTMOVED = now # fake action

    if not MA.ERROR and not MA.HOLD:
      # check if idle/moving
      if xyz_cmp(self.lastwpos,WS.WPOS) and xyz_cmp(self.lastepos,WS.EPOS):
        # no change
        duration = now - MA.LASTMOVED
        threshold = 2.0
        if MA.STATUS not in ("Idle","Check") and duration >= threshold:
          # not moving (for a while)
          MA.SETSTATUS("Idle")
      else:
        # moving
        MA.LASTMOVED = now
        if MA.STATUS == "Idle":
          MA.SETSTATUS("POS") # pos changed during idle
        self.lastwpos = WS.WPOS
        self.lastepos = WS.EPOS

  #--------------------------------------------------------

  # redefine current position with raw G92 (WPOS and EPOS)

  def redefinepos(self, wpos, epos):

    mreq = f"G92 X{wpos[0]:.3f} Y{wpos[1]:.3f} Z{wpos[2]:.3f} E{epos[0]:.3f}"
    self.rawcall(mreq)

    # check we have the position we wanted
    self.refreshpos()
    if xyz_cmp(WS.WPOS,wpos,0.01) and xyz_cmp(WS.EPOS,epos,0.01):
      # position redefined within error margin!
      save_workspace_file()
    else:
      app.error(f"Failed: {mreq}")
      MA.SETSTATUS("ERROR", "Failed to redefine current position with G92")
      self.rawcall("M410") # solve with quick stop (firmware reboot)

  #--------------------------------------------------------

  # limit hit or stall detected (trinamic stallguard)

  def limitstopecho(self, mrespline):

    if MA.LIMITSTOP:
      return # avoid never ending recursion

    # get real-time "stop" position (no check)
    self.refreshpos(False)

    # marlin workaround: garbled pos = real-time pos
    self.redefinepos(WS.WPOS, WS.EPOS)

    MA.SETSTATUS("LIMITSTOP", mrespline)

  #--------------------------------------------------------

  # TODO: At the moment we only support E (one extra axis)

  def abctomarlin(self, mreq):
    mreq = mreq.replace("A", "E")
    start = mreq.find("B")
    if start >= 3:
      end = mreq.find(" ", start)
      if end < start:
        end = len(mreq)
      mreq = mreq[0:start-1] + mreq[end:]
    start = mreq.find("C")
    if start >= 3:
      end = mreq.find(" ", start)
      if end < start:
        end = len(mreq)
      mreq = mreq[0:start-1] + mreq[end:]
    if len(mreq) <= 3:
      return None
    else:
      return mreq

  #--------------------------------------------------------

  # sends "untranslated" marlin request (mreq)
  # returns response lines (mresplines) excluding echo
  # last response line is always "ok" (!)

  # updates STATUS
  # updates WPOS and EPOS if "M114 R"
  # does limitstop detection

  # emergencychecker will be periodically called while waiting for response
  # it may send "M410" to make an emergency stop (will raise Exception)

  def rawcall(self, mreq, emergencychecker=None):

    # pre check...

    if mreq.startswith("G0 G"):
      # ignore grbl special G0 code: G0 G54 G17 G21 G90 G94
      app.info(f"Ignored: {mreq}")
      return ["ok"]

    if mreq == "M0":
      # soft hold or reset
      if MA.STATUS in ("Run","Jog"):
        # client must honor this state = save/hold-back further gcode
        app.note("Control: Hold")
        MA.SETSTATUS("Hold")
      else:
        self.resetstate()
      return ["ok"]

    if mreq == "M108":
      # soft resume or reset
      if MA.STATUS == "Hold":
        app.note("Control: Resume")
        MA.SETSTATUS("Run")
        MA.HOLD = False
      else:
        self.resetstate()
      return ["ok"]

    if mreq == "M410":
      # will quick stop (panic stop)
      app.note("QUICK STOP")
      MA.SETSTATUS("STOP", "Quick stop")
      # send request and raise exception (reconnect)
      self.firmwareio += f"{mreq}\n"
      millisleep(100)
      raise Exception("QUICK STOP")

    if MA.HOLD and mreq.startswith("G"):
      # client has not done its job
      MA.SETSTATUS("ERROR", "Received new gcode while in hold mode")
      app.info(f"Ignored: {mreq}")
      return ["ok"] # throw away

    if MA.ERROR and mreq.startswith("G"):
      app.info(f"Blocked: {mreq}")
      return ["ok"] # throw away

    # lets go...

    if mreq.startswith("G92") and mreq != "G92":
      # we need fresh position first
      self.refreshpos()

    moving = False
    mreq3 = ""
    if len(mreq) > 3:
      mreq3 = mreq[:3]
    else:
      mreq3 = mreq
    if mreq3 in ("G0 ","G1 ","G2 ","G3 ","G5 ", "G6 "):
      # Motion
      mreq = self.abctomarlin(mreq)
      if not mreq:
        MA.SETSTATUS("ERROR", "ABC pos translation error")
        return ["ok"]
      if mreq3 == "G0 ":
        MA.SETSTATUS("Jog")
      else:
        MA.SETSTATUS("Run")
      moving = True
    elif mreq3 == "G28":
      self.call("G54")
      MA.SETSTATUS("Jog", "Homing")
      moving = True
    elif mreq3 == "G29":
      self.call("G54")
      MA.SETSTATUS("Jog", "Bed leveling")
      moving = True

    # send request
    self.firmwareio += f"{mreq}\n"

    # calculate timeout
    if mreq.startswith("M"):
      waitms = int(app.timeout()*1000)
    else:
      waitms = int(app.timeout()*1000*2)
    longms = waitms - 10000

    mresplines = []
    while not self.complete(mresplines):
      waitms -= millisleep(2)
      if moving:
        MA.LASTMOVED = utcnow()
      if waitms <= 0:
        # TIMEOUT!
        MA.SETSTATUS("ERROR", f"TIMEOUT: {mreq}")
        app.note("Machine timeout")
        raise Exception("Machine timeout")
      if waitms == longms:
        app.info(f"long: {mreq}")
      if emergencychecker:
        # may do rawcall("M410") that will raise Exception afterwords
        emergencychecker(True)

    # done (post process)

    if moving:
      # refresh position after every move
      self.refreshpos(False)

    if mreq == "M114 R" and len(mresplines) == 2 and mresplines[1] == "ok":
      # update global WPOS and EPOS
      mresp = mresplines[0]
      MA.POSLINE = mresp
      fields = mresp.split(" ")
      if len(fields) < 4:
        raise Exception(f"M114 output error: {mresp}")
      f0 = fields[0]
      f1 = fields[1]
      f2 = fields[2]
      f3 = fields[3]
      if not f0.startswith("X:") or not f1.startswith("Y:") or not f2.startswith("Z:"):
        raise Exception(f"M114 XYZ output error: {mresp}")
      if not f3.startswith("E:"):
        raise Exception(f"M114 E output error: {mresp}")
      x = gcode_lettervalue(f0)
      y = gcode_lettervalue(f1)
      z = gcode_lettervalue(f2)
      e = gcode_lettervalue(f3)
      WS.WPOS = (x, y, z)
      WS.EPOS = (e, 0.0, 0.0)
      self.refreshedpos = True

    # stepper check
    if mreq == "M17" or mreq.startswith("M17 "):
      MA.DISABLED = False
    elif mreq == "M18" or mreq.startswith("M18 "):
      MA.DISABLED = True

    # unsafe check
    if mreq == "M120" and MA.LIMITSENSORS:
      if MA.LIMITSENSORS() and MA.UNSAFE:
        MA.UNSAFE = False
        MA.CLEAR()
        if MA.MESSAGE == "Disabled limit sensors":
          MA.SETSTATUS("Idle", "Enabled limit sensors")
      if MA.STATUS == "Check":
        MA.STATUS = "Idle"
      app.note("Enabled limit sensors")
    elif mreq == "M121" and MA.LIMITSENSORS:
      if MA.LIMITSENSORS() and not MA.UNSAFE:
        MA.UNSAFE = True
        MA.CLEAR()
        MA.SETSTATUS("Check", "Disabled limit sensors")
      app.warning("Disabled limit sensors")

    if mreq == "G28" or mreq.startswith("G28 "):
      MA.SETSTATUS("Idle", "Homed")
    elif mreq == "G29" or mreq.startswith("G29 "):
      MA.SETSTATUS("Idle", "Leveled")

    return mresplines

  #--------------------------------------------------------

  # called by rawcall

  def complete(self, mresplines):

    self.firmwareio.receive()

    while True:

      mrespline = self.firmwareio.getbymark("\n")
      if not mrespline:
        return False # NOT DONE

      if mrespline.startswith("echo:") or mrespline.startswith("error:"):
        # special response (side channel)
        if not "echo:busy: processing":
          APP.STATUSLOG.addline(mrespline)
        if mrespline.startswith("error:") or mrespline.startswith("echo:busy: paused"):
          app.error(f"firmware {mrespline}")
          MA.SETSTATUS("ERROR", mrespline)
          mresplines = ["ok"]
          return True # ERROR
        elif not MA.UNSAFE and mrespline.startswith("echo:endstop"):
          app.error(f"firmware {mrespline}")
          if not MA.LIMITSTOP:
            self.limitstopecho(mrespline)
            app.note("Will quick stop (limit sensor triggered)")
            self.rawcall("M410") # EMERGENCY STOP
        else:
          app.info(f"firmware {mrespline}")
      else:
        # append gcode response line
        mresplines.append(mrespline)

      if mrespline == "ok":
        return True # DONE!

  #--------------------------------------------------------

  # redefine "specific" workspace position (G10 L20)
  # G54 can only be changed when active (!)

  def g10redefine(self, mreq):

    MA.CLEAR()

    fields = mreq.split(" ")
    if len(fields) < 4:
      return ["ok"]

    pn = fields[2] # workspace 1-6
    if not pn.startswith("P") or len(pn)!=2:
      raise Exception(f"Expected G10 L20 Pn: {mreq}")

    n = int(pn[1])
    grid = n + 53
    if not 54 <= grid <= 59:
      raise Exception(f"Workspace must be P1-P6 (G54-G59): {mreq}")

    if grid==54 and WS.GRID!=54:
      raise Exception(f"Can only change G54 (P1) workpsace when active: {mreq}")

    gpos = WS.GPOS(grid)
    x = gpos[0]
    y = gpos[1]
    z = gpos[2]

    a = None
    b = None
    c = None

    xyzfields = fields[3:]
    for field in xyzfields:
      if field.startswith("X"):
        x = gcode_lettervalue(field)
      elif field.startswith("Y"):
        y = gcode_lettervalue(field)
      elif field.startswith("Z"):
        z = gcode_lettervalue(field)
      elif field.startswith("A"):
        a = gcode_lettervalue(field)
      elif field.startswith("B"):
        b = gcode_lettervalue(field)
      elif field.startswith("C"):
        c = gcode_lettervalue(field)

    if grid == WS.GRID:
      # active workspace => translate G10 into G92
      mreq = f"G92 X{x:.3f} Y{y:.3f} Z{z:.3f}"
      if a is not None and MA.AAXIS:
        mreq += f" E{a:.3f}"
      if b is not None and MA.BAXIS:
        # TODO
        pass
      if c is not None and MA.CAXIS:
        # TODO
        pass
      return self.call(mreq)
    else:
      # passive workspace => adjust offset (apply delta)
      # TODO: verify
      offset = WS.OFFSET(grid)
      delta = xyz_sub(gpos, (x,y,z))
      newoffset = xyz_add(offset, delta)
      WS.SETOFFSET(grid, newoffset)

    save_workspace_file()

  #--------------------------------------------------------

  # sends "processed" marlin request (mreq)
  # returns response lines (mresplines) excluding echo
  # last response line is always "ok" (!)

  # adds "workspace logic" on top of marlin g-code (!)

  # M114 Q => full state: "STATUS:a G:n X:n Y:n Z:n E:n S:n"
  # M114 R => real-time pos for current grid (WPOS)
  # M114 R54 => real-time pos for machine grid (MPOS)
  # M114 R55-59 => real-time pos for explicit offset grid

  # G54 => activate G54 machine grid (1)
  # G55-59 => activate G55-59 offset grid (2-6)
  # G92 => redefine current pos (move current grid's origo)
  # G10 L20 P1-6 => redefine pos in explicit grid

  # emergencychecker will be periodically called while waiting for response
  # it may send "M410" to make an emergency stop (will raise Exception)

  def call(self, mreq, emergencychecker=None):

    mresplines = []

    if mreq == "M2":
      # program done
      # TODO: accurate end time
      # b'[MSG:Pgm End]\r\n'
      return ["ok"]

    # home check
    if mreq == "G28" and not MA.HOMEALL:
      MA.SETSTATUS("ERROR", "Home ALL not allowed (see machine.ini)")
      return ["ok"] # throw away
    elif mreq.startswith("G28 "):
      if not MA.LIMITX and "X" in mreq:
        MA.SETSTATUS("ERROR", "No limit sensor on X axis (see machine.ini)")
        return ["ok"] # throw away
      if not MA.LIMITY and "Y" in mreq:
        MA.SETSTATUS("ERROR", "No limit sensor on Y axis (see machine.ini)")
        return ["ok"] # throw away
      if not MA.LIMITZ and "Z" in mreq:
        MA.SETSTATUS("ERROR", "No limit sensor on Z axis (see machine.ini)")
        return ["ok"] # throw away

    if mreq == "M114 Q":
      # report current state (semi-virtual)
      self.refreshpos()
      # TODO: E1 and E2 axes
      mresplines.append(
        f"STATUS:{MA.STATUS} G:{WS.GRID} X:{WS.WPOS[0]:.3f} Y:{WS.WPOS[1]:.3f} Z:{WS.WPOS[2]:.3f} E:{WS.EPOS[0]:.3f} S:{WS.SVALUE}")
      mresplines.append("ok")

    elif mreq.startswith("G10 L20"):
      # redefine "specific" workspace (semi-virtual)
      APP.STATUSLOG.addline(mreq)
      app.info(f"Workspace change: {mreq}")
      return self.g10redefine(mreq)

    elif mreq == "G54":
      # activate "machine" workspace (semi-virtual)
      if WS.GRID != 54:
        self.refreshpos()
        mpos = WS.MPOS()
        # redefine workspace = mpos
        WS.GRID = 54
        self.redefinepos(mpos, WS.EPOS)
      APP.STATUSLOG.addline(mreq)
      app.info(f"Workspace change: {mreq}")
      mresplines.append("ok")

    elif mreq=="G55" or mreq=="G56" or mreq=="G57" or mreq=="G58" or mreq=="G59":
      # activate "offset" workspace (semi-virtual)
      grid = int(mreq[1:])
      if WS.GRID != grid:
        self.refreshpos()
        mpos = WS.MPOS()
        offset = WS.OFFSET(grid)
        wpos = xyz_sub(mpos, offset) # apply offset
        # redefine workspace = wpos
        WS.GRID = grid
        self.redefinepos(wpos, WS.EPOS)
      APP.STATUSLOG.addline(mreq)
      app.info(f"Workspace change: {mreq}")
      mresplines.append("ok")

    elif mreq!="M114 R" and mreq.startswith("M114 R"):
      # get real-time position of "explicit" workspace (semi-virtual)
      grid = int(mreq[6:])
      if not 54 <= grid <= 59:
        MA.SETSTATUS("ERROR", "Workspace out of range")
        return ["ok"] # throw away
      self.refreshpos()
      mpos = WS.MPOS()
      if grid == 54:
        # no offset
        wpos = mpos
      else:
        # apply offset
        offset = WS.OFFSET(grid)
        wpos = xyz_sub(mpos, offset)
      mresplines.append(f"X:{wpos[0]:.3f} Y:{wpos[1]:.3f} Z:{wpos[2]:.3f} E:{WS.EPOS[0]:.3f}")
      mresplines.append("ok")

    else:
      # pass-through

      mresplines = self.rawcall(mreq, emergencychecker)

      # workspace adjustment?
      if mreq.startswith("G92") and mreq != "G92":
        if WS.GRID == 54:
          # machine workspace => refresh after G92
          self.refreshpos()
          save_workspace_file()
        else:
          # adjust workspace offset
          fields = mreq.split(" ")
          x = WS.WPOS[0]
          y = WS.WPOS[1]
          z = WS.WPOS[2]
          for field in fields:
            if field.startswith("X"):
              x = gcode_lettervalue(field)
            elif field.startswith("Y"):
              y = gcode_lettervalue(field)
            elif field.startswith("Z"):
              z = gcode_lettervalue(field)
          offset = WS.OFFSET(WS.GRID)
          delta = xyz_sub(WS.WPOS, (x,y,z))
          newoffset = xyz_add(offset, delta)
          WS.SETOFFSET(WS.GRID, newoffset)
          WS.WPOS = (x,y,z)
          save_workspace_file()
      if mreq.startswith("G92"):
        self.refreshpos()

    return mresplines

#==========================================================
