#
# Global application static/config/data/state
#
# GPLv3 (C) 2021 librehof.com
#

# establish application's root and source directories
import os
import sys
import platform
import threading
os.chdir(os.path.dirname(os.path.realpath(__file__)))
APPSOURCE = os.getcwd()
os.chdir(os.path.dirname(APPSOURCE))
APPROOT = os.getcwd()
APPHOST = platform.node()
if not APPHOST:
  APPHOST = "host"
sys.path.append(f"{APPSOURCE}/common")
sys.path.append(f"{APPSOURCE}/regular")
sys.path.append(f"{APPSOURCE}/web")

from common import *

#==========================================================

# global app variables

class APP:

  # app meta

  TITLE = "MarlinDroid"
  LABEL = loadutf("meta/label.var").replace("\n", "")
  VERSION = loadutf("meta/version.var").replace("\n", "")

  # web meta (setupweb)

  UTFEXTS = { } # { ".ext": "textmediatype" }
  RAWEXTS = { } # { ".ext": "rawmediatype" }

  #--------------------------------------------------------

  # app config

  DEV = False # debug output, longer timeouts, ...
  SERVICE = False # started by a service manager
  ORDINAL = 0
  if pathexists("config/ordinal.var"):
    ORDINAL = int(loadutf("config/ordinal.var").replace("\n", ""))

  TIMEOUT = 40
  DEVTIMEOUT = 80
  BUFMAXMB = 1024
  LOGMAXKB = 500

  LOGFILE = "log/app.log"

  # web config

  WEBNIC = None
  WEBPORT = 8000 + ORDINAL

  WEBPOLLMS = 300
  WEBMAXOPEN = 10
  WEBCHECKPORT = True

  WEBLOGFILE = None

  # app specific config

  MODE = "grbl2marlin"

  GNIC = "127.0.0.1"
  GPORT = 2000 + ORDINAL

  FIRMWARETTY = "/dev/ttyACM*"
  FIRMWAREBAUD = 115000

  FIRMWAREIP = None
  FIRMWAREPORT = 23

  CLIENTLOGFILE = None
  FIRMWARELOGFILE = None
  INNERLOGFILE = None
  STATUSLOGFILE = None

  #--------------------------------------------------------

  # app state

  LOCK = threading.RLock() # global lock

  EXITAT = None # exit when TICK > EXITAT, see app.exit()
  EXITMETHOD = None # "SHUTDOWN" (0), "RESTART" (1), "ERROREXIT" (2)

  GOVERNOR = None # governor thread

  # app aware threads (daemon=False)
  THREADS = { } # thread = tick (updated when calling milliping)
  STALLED = None # thread (with longest silence) not responding

  # light threads, governor will lock and execute ticker.tick()
  TICKERS = set() # set of weakref.ref(ticker)

  LOG = None # = FileLog()

  # web state

  WEBLOG = None # = FileLog()
  WEBSERVER = None # = WebServer()
  WEBRELOADED = False # use to reload web page (dev mode)

  # app specific state

  FIRMWARELOG = None # = Logfile()
  CLIENTLOG = None # = Logfile()
  INNERLOG = None # = Logfile()
  STATUSLOG = None # = Logfile()

  GLISTENER = None
  CLIENTIO = None
  FIRMWAREIO = None
  FIRMWARECON = ""

#==========================================================

# default app.ini

def app_ini():

  #-------------------------------
  return f'''\
# {APP.LABEL} application config

# *** generic ***

# development mode (debug)
DEV=False

# general timeout and max buffer and log
#TIMEOUT={APP.TIMEOUT}
#DEVTIMEOUT={APP.DEVTIMEOUT}
#BUFMAXMB={APP.BUFMAXMB}
#LOGMAXKB={APP.LOGMAXKB}

# application log
LOGFILE="{APP.LOGFILE}"
#LOGFILE=None

# *** firmware ***

# firmware via serial device
FIRMWARETTY="/dev/ttyACM*" # USB to serial (standard)
#FIRMWARETTY="/dev/ttyUSB*" # USB to serial (special driver)
#FIRMWARETTY="/dev/ttyS0" # serial device
#FIRMWARETTY="COM1" # COM0..N on windows
FIRMWAREBAUD={APP.FIRMWAREBAUD}

# firmware via LAN network
FIRMWAREIP=None
#FIRMWAREIP="n.n.n.n"
FIRMWAREPORT={APP.FIRMWAREPORT}

# *** web ***

WEBNIC="127.0.0.1" # listen on host's local interface
#WEBNIC="0.0.0.0" # listen on all network interfaces
#WEBNIC=None

# port for browser or web proxy to connect to
# default = 8000 + config/ordinal.var (0)
# WEBPORT=8000

#WEBPOLLMS={APP.WEBPOLLMS} # browser poll time [ms]
#WEBMAXOPEN={APP.WEBMAXOPEN} # max active connections
#WEBCHECKPORT=True # check port is not in use

WEBLOGFILE=None
#WEBLOGFILE="log/web.log"

# *** app ***

MODE="grbl2marlin"
#MODE="marlin2marlin"
#MODE="simple"

#GNIC="0.0.0.0" # listen on all network interfaces
GNIC="127.0.0.1" # listen on computer local interface

# port for gcode client to connect to
# default = 2000 + config/ordinal.var (0)
# GPORT=2000

# machine status log
STATUSLOGFILE="log/status.log"

# inner gcode log (processed gcode)
INNERLOGFILE=None
#INNERLOGFILE="log/inner.log"

# raw client gcode log (client side debug)
CLIENTLOGFILE=None
#CLIENTLOGFILE="log/client.log"

# raw firmware gcode log (firmware side debug)
FIRMWARELOGFILE=None
#FIRMWARELOGFILE="log/firmware.log"
''' #-----------------------------

#----------------------------------------------------------

def load_app_ini():

  # default
  if not pathexists("config/app.ini"):
    content = app_ini()
    saveutf8("config/app.ini", content)

  # load
  loadconfig(APP, "config/app.ini")

#==========================================================

# global machine variables

class MA:

  # machine meta/config

  # from M115
  FIRMWARENAME = "Firmware"
  FIRMWAREVERSION = "0.0"
  FIRMWARETIMESTAMP = ""
  FIRMWARECAPS = { } # marlin firmware capabilities
  MACHINEDATA = { } # machine type info

  # M115 overrides
  FORCENAME = ""
  FORCEUUID = ""

  # machine's work area [mm]
  DIMX = 2000
  DIMY = 2000
  DIMZ = 200

  # grbl A,B,C axis
  AAXIS = True  # marlin E(0)
  BAXIS = False # marlin E1
  CAXIS = False # marlin E2

  # max jog rate [mm/min]
  JOGMAX = None # None = use firmware's mm/min

  # existing limit sensors
  LIMITX = True
  LIMITY = True
  LIMITZ = True

  # allow home without specifing axes
  HOMEALL = True

  @staticmethod
  def LIMITSENSORS():
    if MA.LIMITX or MA.LIMITY or MA.LIMITZ:
      return True
    else:
      return False

  # S tool value (M3 S)
  SVALUEMIN = 100
  SVALUEMAX = 1000
  SVALUEDEFAULT = 800

  # real tool output (S value translation)
  TOOLDECIMALS = 1
  TOOLUNIT = "%"
  TOOLMAX = 100

  #--------------------------------------------------------

  @staticmethod
  def MACHINENAME():
    if MA.FORCENAME:
      return MA.FORCENAME
    elif "MACHINE_TYPE" in MA.MACHINEDATA:
      return MA.MACHINEDATA["MACHINE_TYPE"]
    else:
      return "Machine"

  @staticmethod
  def MACHINEUUID():
    if MA.FORCEUUID:
      return MA.FORCEUUID
    elif "UUID" in MA.MACHINEDATA:
      return MA.MACHINEDATA["UUID"]
    else:
      return "00000000-0000-0000-0000-000000000000"

  @staticmethod
  def MACHINEINFO():
    return f"{MA.MACHINENAME()} ({MA.MACHINEUUID()})"

  @staticmethod
  def FIRMWAREINFO():
    if not MA.FIRMWARETIMESTAMP:
      return f"{MA.FIRMWARENAME} {MA.FIRMWAREVERSION}"
    else:
      return f"{MA.FIRMWARENAME} {MA.FIRMWAREVERSION} ({MA.FIRMWARETIMESTAMP})"

  #--------------------------------------------------------

  # machine state

  # Standard grbl status:
  # - Idle
  # - Run/Jog
  # - Check => just connected and stepper counters are zero
  # - Hold => will not "ok" gcode (except resume)
  # Warnings:
  # - Offline
  # - POS => position changed without Run/Jog (workspace change)
  # Errors (will "ok" but throw away moves)
  # - STOP
  # - LIMITSTOP then LIMIT when signal goes away
  # - ERROR

  STATUS = "Unknown"
  STATUSTIME = "" # hh:m:ss

  # marlin raw position info
  POSLINE = ""

  # sticky status
  LASTMOVED = 0 # unix time
  HOLD = False # will not ok gcode until resume
  STOP = False # quick stop
  LIMITSTOP = False # limit sensor triggered, from echo or poll
  ERROR = False # from "error:message"

  # last message (stays until Jog/Run)
  MESSAGE = "" # Status message
  MSGTIME = "" # hh:m:ss

  # special machine states (stays until Jog/Run)
  STARTED = "" # start of program as hh:m:ss (idle=>jog/run)
  ENDED = "" # end of program as hh:m:ss (jog/run=>idle)
  UNSAFE = False # limit sensors disabled
  DISABLED = False # disabled steppers

  # clear sticky
  @staticmethod
  def CLEAR():
    MA.LASTMOVED = 0
    MA.HOLD = False # waiting for resume
    MA.ERROR = False
    MA.STOP = False # sub-error
    MA.LIMITSTOP = False # sub-error

  # set global machine status
  @staticmethod
  def SETSTATUS(status, message=None):

    now = utcnow()
    nowdt = hostnowdt()
    hms = nowdt.strftime("%H:%M:%S")

    if status in ("Jog","Run"):
      MA.DISABLED = False
      MA.LASTMOVED = now # moving

    # message
    logmessage = None
    if message and message != MA.MESSAGE:
      if not MA.ERROR or not MA.MESSAGE:
        MA.MSGTIME = hms
        MA.MESSAGE = message
        logmessage = message

    if status == MA.STATUS:
      return # skip

    # sticky status
    if status in ("Jog","Run") and MA.STATUS == "Idle":
      MA.CLEAR()
      MA.STARTED = hms
      MA.ENDED = ""
      if not logmessage:
        MA.MSGTIME = hms
        MA.MESSAGE = ""
    elif status == "Idle" and MA.STATUS in ("Jog","Run"):
      MA.ENDED = hms
    elif status == "Hold" and not MA.ERROR:
      MA.HOLD = True
    elif status in ("ERROR","STOP","LIMIT","LIMITSTOP"):
      MA.ERROR = True
      MA.HOLD = False
      if status == "LIMITSTOP":
        MA.LIMITSTOP = True
      if status == "STOP":
        MA.STOP = True

    if status == "Idle" and MA.STATUS == "Check":
      return

    # new status
    MA.STATUS = status
    MA.STATUSTIME = hms

    line = status
    if logmessage:
      line += ": " + logmessage
    APP.STATUSLOG.addline(line)

#==========================================================

def machine_ini():

  #-------------------------------
  return f'''\
# {APP.TITLE} machine config

# overrides M115
#FORCENAME="MyMachine"
#FORCEUUID="{str(uuid.uuid4())}"

# machine's work area [mm]
DIMX=2000
DIMY=2000
DIMZ=200

# grbl A,B,C axis
AAXIS=True  # marlin E(0)
BAXIS=False # marlin E1
CAXIS=False # marlin E2

# max jog rate [mm/min]
JOGMAX=None # None=firmware's mm/min

# existing limit sensors
LIMITX=True
LIMITY=True
LIMITZ=True

# allow home without specifing axes
HOMEALL=True

# S tool value (M3 S)
SVALUEMIN=100
SVALUEMAX=1000
SVALUEDEFAULT=800

# real tool output (S value translation)
TOOLDECIMALS=1
TOOLUNIT="%"
TOOLMAX=100
''' #-----------------------------

#----------------------------------------------------------

def load_machine_ini():

  # default
  if not pathexists("config/machine.ini"):
    content = machine_ini()
    saveutf8("config/machine.ini", content)

  # load
  loadconfig(MA, "config/machine.ini")

#==========================================================

# global workspace variables

class WS:

  # workspace state

  VALID = False # will not save if pos is invalid

  # current grid (machine grid 54 or offset grid 55-59)
  GRID=54

  # X,Y,Z in current work grid (54-59) = MPOS - OFFSET
  WPOS = (0.0, 0.0, 0.0)

  # A/E0,B/E1,C/E2 extended positions (machine grid only)
  EPOS = (0.0, 0.0, 0.0)

  # S-tool output (M3 S)
  SVALUE = 0

  # grid offsets
  OFFSET55 = (0.0, 0.0, 0.0)
  OFFSET56 = (0.0, 0.0, 0.0)
  OFFSET57 = (0.0, 0.0, 0.0)
  OFFSET58 = (0.0, 0.0, 0.0)
  OFFSET59 = (0.0, 0.0, 0.0)

  #--------------------------------------------------------

  # XYZ in machine grid (54): MPOS = WPOS + OFFSET
  @staticmethod
  def MPOS():
    if WS.GRID == 54:
      return WS.WPOS
    else:
      offset = WS.OFFSET()
      mpos = xyz_add(WS.WPOS, offset)
      return mpos

  # XYZ in explicit grid: GPOS = MPOS - OFFSET(grid)
  @staticmethod
  def GPOS(grid):
    mpos = WS.MPOS()
    offset = WS.OFFSET(grid)
    gpos = xyz_sub(mpos, offset)
    return gpos

  # XYZ work coordinate offset: WCO = MPOS - WPOS
  @staticmethod
  def WCO():
    mpos = WS.MPOS()
    wco = xyz_sub(mpos, WS.WPOS)
    return wco

  # XYZ machine-local offset = MPOS - GPOS (default GPOS = WPOS)
  @staticmethod
  def OFFSET(grid=None):
    if not grid:
      grid = WS.GRID
    if grid == 54:
      return (0.0, 0.0, 0.0) # always zero for machine grid
    elif grid == 55:
      return WS.OFFSET55
    elif grid == 56:
      return WS.OFFSET56
    elif grid == 57:
      return WS.OFFSET57
    elif grid == 58:
      return WS.OFFSET58
    elif grid == 59:
      return WS.OFFSET59

  # set XYZ offset
  @staticmethod
  def SETOFFSET(grid, offset):
    if not grid:
      raise Exception("SETOFFSET: No grid")
    if grid == 54:
      raise Exception("Can not set offset for G54 (machine grid)")
    elif grid == 55:
      WS.OFFSET55 = offset
    elif grid == 56:
      WS.OFFSET56 = offset
    elif grid == 57:
      WS.OFFSET57 = offset
    elif grid == 58:
      WS.OFFSET58 = offset
    elif grid == 59:
      WS.OFFSET59 = offset

#==========================================================

def workspace_file():

  #-------------------------------
  return f'''\
# {APP.TITLE} workspace

# M114 Q => full state: "STATUS:a G:n X:n Y:n Z:n E:n S:n" 
# M114 R => real-time pos for current grid (WPOS)
# M114 R54 => real-time pos for machine grid (MPOS)
# M114 R55-59 => real-time pos for explicit offset grid

# G54 => activate G54 machine grid (1)
# G55-59 => activate G55-59 offset grid (2-6)
# G92 => redefine current pos (move current grid's origo)

# current grid (machine grid 54 or offset grid 55-59)
GRID={WS.GRID}

# X,Y,Z in current work grid (54-59) = MPOS + OFFSET
WPOS=({xyz_str(WS.WPOS)})

# A/E0,B/E1,C/E2 extra positions (always part of machine grid)
EPOS=({xyz_str(WS.EPOS)})

# S-tool output (M3 S)
SVALUE={WS.SVALUE}

# grid offsets (WPOS = MPOS + OFFSET)
OFFSET55=({xyz_str(WS.OFFSET55)})
OFFSET56=({xyz_str(WS.OFFSET56)})
OFFSET57=({xyz_str(WS.OFFSET57)})
OFFSET58=({xyz_str(WS.OFFSET58)})
OFFSET59=({xyz_str(WS.OFFSET59)})
''' #-----------------------------

#----------------------------------------------------------

def load_workspace_file():

  # default workspace
  if not os.path.exists("config/workspace.ini"):
    content = workspace_file()
    saveutf8("config/workspace.ini", content)

  # enforce workspace state
  if not os.path.exists("data/workspace.data"):
    copyfile("config/workspace.ini", "data/workspace.data")

  # load workspace state
  loadconfig(WS, "data/workspace.data")

#----------------------------------------------------------

def save_workspace_file():

  if not WS.VALID:
    return

  print("Saving workspace")
  saved = loadutf8("data/workspace.data")
  current = workspace_file()
  if current != saved:
    saveutf8("data/workspace.data", current)

#==========================================================
